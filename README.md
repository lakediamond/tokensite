# Tokensite

![tokensite-infra](/uploads/c23a40fba14b7988d6da38026cf04077/tokensite-infra.png)

Contains:
- App: frontend in nodejs
- Api: backend in nodejs for the logics and blockchain connections
- Mysql: database

## Deploy locally to test

- Checkout branch staging: `git checkout staging`
- Start the stack: 
```
  TAG=local \
  API=http://localhost:8080 \
  MYSQL_PASSWORD="xxx" \
  MYSQL_ROOT_PASSWORD="xxx..." \
  docker-compose \
      up -d --build
```
- To not rebuild everytime, remove the `--build`
- Initialize db schema tables with: `docker exec -it api npm run migrate`
- Load dummy data for dev/test only: `docker exec -it api npm run seed:all`

- Check on url [tokensite](http://localhost:3000)
  - Use default login: `alice@demo.com` , password: `654321`
  - (Or create your own user + eth wallet) <-- feature needs to be tested!

- Connect alice wallet via metamask.

- Create new demo orders: 
  - Fix some dependency: `docker exec api npm install --prefix order`
  - Creates orders: `docker exec api node order/blockchain.js` (order should appears after 5min)

- Click on an order and make a proposal for LKD. 
- Proposal should appears in history > my activity

- To delete all the stack (container + db): 
```
  docker-compose stop && \
  docker-compose rm -f && \
  docker volume rm tokensite_mysql_data
```

## Ethereum info

Code is [here](https://gitlab.com/lakediamond/tokensite-smartcontract)



