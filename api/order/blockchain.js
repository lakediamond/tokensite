const fs = require('fs'),
    Web3 = require('web3'),
    config = require('./config.json'),
    abi = require('./abi.json'),
    Tx = require('ethereumjs-tx'),
    privateKey = fs.readFileSync(__dirname + '/privatekey.txt').toString();

const etherToWei = 1000000000000000000;
const provider = new Web3.providers.HttpProvider(config.HttpProvider)
const web3 = new Web3(provider);

const TMTContract = new web3.eth.Contract(abi, config.contract.address);

const account = web3.eth.accounts.privateKeyToAccount("0x" + privateKey);

console.log(account)
console.log(privateKey)

const checkConnection = () => {
    return web3.eth.net.isListening();
}

const checkETHBalance = () => {
    return web3.eth.getBalance(account.address);
}

const checkTMTBalance = () => {
    return TMTContract.methods.balanceOf(account.address).call();
}

const openOrder = async (type, metadata, etherToSend, nonce) => {
    let txBuffer = new Buffer(privateKey, 'hex')
    let payloadData = TMTContract.methods.createOrder(type, metadata).encodeABI();
    
    let rawTx = {
        nonce:  nonce,
        gasPrice: web3.utils.toHex(10000000000), 
        gasLimit: web3.utils.toHex(7000000),
        from:   account.address,
        to: config.contract.address,
        value: etherToSend * etherToWei,
        data:   payloadData
    };

    let tx = new Tx(rawTx)
    tx.sign(txBuffer)
    var serializedTx = tx.serialize();

    return web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex')).on('receipt', console.log);
}

(async () => {
    await checkConnection().then(console.log.bind({}, 'connection'));
    await checkETHBalance().then(console.log.bind({}, 'eth'));
    await checkTMTBalance().then(console.log.bind({}, 'lkd'));

    let types = ["plate_micromechanical", "plate_laser", "plate_transistors", "plate_photonics"];
    let typePrices = [21, 23, 22, 19];

    var nonce = web3.utils.toHex(await web3.eth.getTransactionCount(account.address))

    openOrder('plate_micromechanical', 'blah blah', 0.1, nonce)
    openOrder('plate_micromechanical', 'blah blah', 0.12, nonce + 1)
    openOrder('plate_laser', 'blah blah', 0.15, nonce + 2)
    openOrder('plate_transistors', 'blah blah', 0.17, nonce + 3)

    try {
        await openOrder('plate_photonics', 'blah blah', 0.2, nonce + 4)
    } catch (e) {
        console.log(e);
    }

    try {
        let count = await TMTContract.methods.orderCounter().call();
        for (let i = 0; i < parseInt(count.toString()); i ++) {
            await TMTContract.methods.orders(i).call().then(console.log)
        }
    } catch (e) {
        console.log(e);
    }
})();
