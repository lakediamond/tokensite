const fs = require('fs'),
    Web3 = require('web3'),
    config = require('./config.json'),
    abi = require('./abi.json'),
    Tx = require('ethereumjs-tx'),
    privateKey = fs.readFileSync(__dirname + '/privatekey.txt').toString();

const provider = new Web3.providers.HttpProvider(config.HttpProvider)
const web3 = new Web3(provider);

const TMTContract = new web3.eth.Contract(abi, config.contract.address);

const account = web3.eth.accounts.privateKeyToAccount("0x" + privateKey);

console.log(account)
console.log(privateKey)

const checkConnection = () => {
    return web3.eth.net.isListening();
}

const addType = async (type, tokens, nonce) => {
    let txBuffer = new Buffer(privateKey, 'hex')
    let payloadData = TMTContract.methods.addType(type, tokens).encodeABI();

      
    // let nonce = web3.utils.toHex(await web3.eth.getTransactionCount(account.address))
    let rawTx = {
        nonce:  nonce,
        gasPrice: web3.utils.toHex(1000000000), 
        gasLimit: web3.utils.toHex(7000000),
        from:   account.address,
        to: config.contract.address,
        data:   payloadData
    };

    let tx = new Tx(rawTx)
    tx.sign(txBuffer)
    var serializedTx = tx.serialize();

    return web3.eth.sendSignedTransaction('0x' + serializedTx.toString('hex')).on('receipt', console.log);
}

(async () => {
    await checkConnection().then(console.log.bind({}, 'connection'));

    let types = ["platelets_micromechanical", "platelets_laser", "platelets_transistors", "platelets_photonics"];
    let typePrices = [100, 200, 300, 400];

    var nonce = web3.utils.toHex(await web3.eth.getTransactionCount(account.address))

    for (let i = 0; i < types.length; i++) {
      try {
        console.log("trying to add types")
        addType(types[i], typePrices[i], nonce);
        nonce++
      } catch (e) {
        console.log(e);
      }
    }
})();
