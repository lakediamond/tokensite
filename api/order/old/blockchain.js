const fs = require('fs'),
    Web3 = require('web3'),
    config = require('./config.json'),
    abi = require('./abi.json'),
    privateKey = fs.readFileSync(__dirname + '/privatekey.txt').toString();

const etherToWei = 1000000000000000000;

const provider = new Web3.providers.HttpProvider(config.HttpProvider)
const web3 = new Web3(provider);

const TMTContract = new web3.eth.Contract(abi, config.contract.address);

const account = web3.eth.accounts.wallet.add(privateKey);

const checkConnection = () => {
    return web3.eth.net.isListening();
}

const checkETHBalance = () => {
    return web3.eth.getBalance(account.address);
}

const checkTMTBalance = () => {
    return TMTContract.methods.balanceOf(account.address).call();
}

const openOrder = async (type, metadata, etherToSend) => {
    let order = TMTContract.methods.createOrder(type, metadata);

    let gasEstimate = await order.estimateGas();
    return order.send({
        from: account.address,
        value: etherToSend * etherToWei,
        gas: gasEstimate + 100000,
        gasPrice: config.contract.gasPrice,
    });
}

(async () => {
    await checkConnection().then(console.log.bind({}, 'connection'));
    await checkETHBalance().then(console.log.bind({}, 'eth'));
    await checkTMTBalance().then(console.log.bind({}, 'tmt'));

    try {
        await openOrder('platelets_micromechanical', 'blah blah', 0.019).then(console.log);
    } catch (e) {
        console.log(e);
    }

    try {
        await openOrder('platelets_laser', 'blah blah', 0.052).then(console.log);
    } catch (e) {
        console.log(e);
    }

    try {
        await openOrder('platelets_transistors', 'blah blah', 0.026).then(console.log);
    } catch (e) {
        console.log(e);
    }

    try {
        await openOrder('platelets_photonics', 'blah blah', 0.011).then(console.log);
    } catch (e) {
        console.log(e);
    }
})();
