module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.changeColumn(
            'Orders',
            'timestamp',
            { type: Sequelize.DATE },
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.changeColumn(
            'Orders',
            'timestamp',
            { type: Sequelize.INTEGER },
        );
    }
}
