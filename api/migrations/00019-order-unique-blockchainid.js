module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addConstraint(
            'Orders',
            ['blockchain_id'],
            {
                type: 'unique',
                name: 'unique-order-blockchain-id'
            }
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeConstraint(
            'Orders',
            'unique-order-blockchain-id',
        );
    }
}
