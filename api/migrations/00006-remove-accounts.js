module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn('Bids', 'account_id').then(() => {
            return queryInterface.dropTable('Accounts');
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Accounts', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING
            },
            type: {
                type: Sequelize.ENUM('private', 'business')
            },
            number: {
                type: Sequelize.INTEGER
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        }).then(() => {
            queryInterface.addColumn('Bids', 'account_id', {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Accounts',
                    key: 'id'
                }
            });
        });
    }
};
