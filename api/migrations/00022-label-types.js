module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Types',
            'label',
            Sequelize.STRING
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn(
            'Types',
            'label',
        );
    }
}
