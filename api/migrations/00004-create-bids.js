'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Bids', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            account_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Accounts',
                    key: 'id'
                }
            },
            amount: {
                type: Sequelize.INTEGER
            },
            trust: {
                type: Sequelize.ENUM('sufficient', 'perfect')
            },
            status: {
                type: Sequelize.ENUM('active', 'pending', 'closed')
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Bids');
    }
};
