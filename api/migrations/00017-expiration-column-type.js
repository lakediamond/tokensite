module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.changeColumn(
            'Orders',
            'expiration',
            { type: Sequelize.DATE },
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.changeColumn(
            'Orders',
            'expiration',
            { type: Sequelize.INTEGER },
        );
    }
}
