module.exports = {
    up: (queryInterface, Sequelize) => {
        return [
            queryInterface.removeColumn(
                'Orders',
                'type'
            ),
            queryInterface.removeColumn(
                'Orders',
                'timestamp'
            ),
            queryInterface.removeColumn(
                'Orders',
                'tmt_needed'
            ),
            queryInterface.addColumn(
                'Orders',
                'blockchain_id',
                Sequelize.INTEGER
            ),
            queryInterface.addColumn(
                'Orders',
                'expiration',
                Sequelize.INTEGER
            ),
            queryInterface.addColumn(
                'Orders',
                'metadata',
                Sequelize.STRING
            )
        ];
    },
    down: (queryInterface, Sequelize) => {
        return [
            queryInterface.addColumn(
                'Orders',
                'type',
                Sequelize.ENUM('platelet', 'round brilliant')
            ),
            queryInterface.addColumn(
                'Orders',
                'timestamp',
                Sequelize.INTEGER
            ),
            queryInterface.addColumn(
                'Orders',
                'tmt_needed',
                Sequelize.INTEGER
            ),
            queryInterface.removeColumn(
                'Orders',
                'blockchain_id'
            ),
            queryInterface.removeColumn(
                'Orders',
                'expiration'
            ),
            queryInterface.removeColumn(
                'Orders',
                'metadata'
            )
        ];
    }
}
