module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn(
            'Bids',
            'active'
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Bids',
            'active',
            Sequelize.BOOLEAN
        );
    }
}
