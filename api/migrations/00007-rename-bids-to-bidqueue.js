module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.renameTable('Bids', 'QueuedBids');
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.renameTable('QueuedBids', 'Bids');
    }
};
