module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Bids',
            'tx_id',
            Sequelize.STRING
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn(
            'Bids',
            'tx_id',
        );
    }
}
