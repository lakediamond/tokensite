module.exports = {
    up: (queryInterface, Sequelize) => {
        const dateField = {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.literal('NOW()'),
        };

        return queryInterface.changeColumn('Addresses', 'createdAt', dateField)
                    .then(() => queryInterface.changeColumn('Addresses', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('Bids', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('Bids', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('Orders', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('Orders', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('QueuedBids', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('QueuedBids', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('Users', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('Users', 'updatedAt', dateField))
    },
    down: (queryInterface, Sequelize) => {
        const dateField = {
            allowNull: false,
            type: Sequelize.DATE,
        };

        return queryInterface.changeColumn('Addresses', 'createdAt', dateField)
                    .then(() => queryInterface.changeColumn('Addresses', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('Bids', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('Bids', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('Orders', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('Orders', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('QueuedBids', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('QueuedBids', 'updatedAt', dateField))
                    .then(() => queryInterface.changeColumn('Users', 'createdAt', dateField))
                    .then(() => queryInterface.changeColumn('Users', 'updatedAt', dateField))
    }
};
