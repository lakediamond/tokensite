module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.removeColumn(
            'Orders',
            'filled'
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Orders',
            'filled',
            Sequelize.DOUBLE
        );
    }
}
