module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Orders',
            'type_id',
            {
                type: Sequelize.INTEGER,
                references: {
                    model: 'Types',
                    key: 'id'
                }
            }
        );
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn(
            'Orders',
            'type_id'
        );
    }
}
