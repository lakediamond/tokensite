module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addConstraint(
            'Bids',
            ['tx_id'],
            {
                type: 'unique',
                name: 'unique-bid-tx-id'
            }
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeConstraint(
            'Bids',
            'unique-bid-tx-id',
        );
    }
}
