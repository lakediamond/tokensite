module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addConstraint(
            'Users',
            ['email'],
            {
                type: 'unique',
                name: 'unique-user-email'
            }
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeConstraint(
            'Users',
            'unique-user-email',
        );
    }
}
