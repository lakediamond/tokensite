module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Bids',
            'status',
            Sequelize.STRING,
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn(
            'Bids',
            'status',
        );
    }
}
