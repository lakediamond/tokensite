module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Orders',
            'conversion',
            Sequelize.DOUBLE
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeColumn(
            'Orders',
            'conversion',
        );
    }
}
