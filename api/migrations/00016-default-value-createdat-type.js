module.exports = {
    up: (queryInterface, Sequelize) => {
        const dateField = {
            allowNull: false,
            type: Sequelize.DATE,
            defaultValue: Sequelize.literal('NOW()'),
        };

        return queryInterface.changeColumn('Types', 'createdAt', dateField)
            .then(() => queryInterface.changeColumn('Types', 'updatedAt', dateField))
    },
    down: (queryInterface, Sequelize) => {
        const dateField = {
            allowNull: false,
            type: Sequelize.DATE,
        };

        return queryInterface.changeColumn('Types', 'createdAt', dateField)
            .then(() => queryInterface.changeColumn('Types', 'updatedAt', dateField))
    }
};
