module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addColumn(
            'Bids',
            'active',
            Sequelize.BOOLEAN
        );
    },

    down: (queryInterface, Sequelize) => {
        queryInterface.removeColumn(
            'Bids',
            'active'
        );
    }
}
