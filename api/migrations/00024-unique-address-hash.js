module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.addConstraint(
            'Addresses',
            ['hash'],
            {
                type: 'unique',
                name: 'unique-address-hash'
            }
        );
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.removeConstraint(
            'Addresses',
            'unique-address-hash',
        );
    }
}
