'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Types', [
            {
                id: 1,
                name: 'plate',
                label: 'Plate',
                tmt_needed: 10,
            },
            {
                id: 2,
                name: 'round brilliant',
                label: 'Round Brilliant',
                tmt_needed: 40,
            },
            {
                id: 3,
                name: 'plate_laser',
                label: 'Plate for lasers',
                tmt_needed: 23,
            },
            {
                id: 4,
                name: 'plate_transistors',
                label: 'Plate for transistors',
                tmt_needed: 22,
            },
            {
                id: 5,
                name: 'plate_photonics',
                label: 'Plate for photonics',
                tmt_needed: 19,
            },
            {
                id: 6,
                name: 'plate_micromechanical',
                label: 'Plate for micromechanical parts',
                tmt_needed: 21,
            },
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Types', null, {});
    }
};
