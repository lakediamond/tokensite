'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Addresses', [
            {
                id: 1,
                hash: '0x66459C02f4459cab5DBB77f487190D85C8299Baf',
                user_id: '1',
            },
            {
                id: 2,
                hash: '0x9999888888887777666663333',
                user_id: '1',
            },
            {
                id: 3,
                hash: '0x134459C02f4459cab5DBB77f487190D85C8299Baf',
                user_id: '2',
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Addresses', null, {});
    }
};
