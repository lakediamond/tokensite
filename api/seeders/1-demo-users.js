'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Users', [
            {
                id: 1,
                firstName: 'Alice',
                lastName: 'Demo',
                email: 'alice@demo.com',
                password: '654321',
            },
            {
                id: 2,
                firstName: 'Romain',
                lastName: 'Braud',
                email: 'romain.braud@lakediamond.ch',
                password: '654321',
            },
            {
                id: 3,
                firstName: 'Greg',
                lastName: 'Boccard',
                email: 'greg.boccard@lakediamond.ch',
                password: '654321',
            }
        ], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Users', null, {});
    }
};
