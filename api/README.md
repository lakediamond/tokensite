## Setup
* `npm install`
* Copy `.env-template` to `.env`
* Update variables in `.env`
* `npm run migrate`
* `npm run seed:all`
* `npm run sync`
* `npm start`
