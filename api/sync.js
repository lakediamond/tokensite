require('dotenv').config();

const Sequelize = require('sequelize');

console.log(process.env.DB_USERNAME);
console.log(process.env.DB_PASSWORD);
var sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
        host: '127.0.0.1',
        dialect: 'mysql',
        operatorsAliases: false,
    }
);
require('./lib/history');