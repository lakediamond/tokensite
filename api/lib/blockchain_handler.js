const Web3 = require('web3'),
    config = require('./config.json'),
    abi = require('./abi.json');

const provider = new Web3.providers.HttpProvider(config.HttpProvider)
const web3 = new Web3(provider);
const TMTContract = web3.eth.contract(abi).at(config.contract.address);

function blockchainConnection() {
    let utc = new Date().toUTCString();
    console.log(utc + ' Connected to blockchain node (' + config.HttpProvider + '): ' + web3.isConnected());
}

console.log('Using web3 version ' + web3.version.api);

module.exports = {
    TMTContract,
    blockchainConnection,
}
