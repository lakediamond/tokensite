const Sequelize = require('sequelize'),
            models = require('../models');


const ETH_TO_WEI = 1000000000000000000;

const addOrder = async (res) => {
    const orderObject = {
        id: res.args.orderCounter.toString(10),
        type: res.args.theType,
        expiration: res.args.expirationDate.toString(10) * 1000,
        conversion: res.args.conversion.toString(10) / ETH_TO_WEI,
        metadata: res.args.metadata,
    }
    const type = await models.Types.findOne({
        where: {
            name: orderObject.type
        }
    });
    if (type == null) {
        console.log('[!] Order type "' + orderObject.type + '" does not exist in the database!');
        return;
    }
    try {
        const order = await models.Orders.create({
            blockchain_id: orderObject.id,
            type_id: type.id,
            expiration: new Date(orderObject.expiration),
            conversion: orderObject.conversion,
            metadata: orderObject.metadata
        });
        console.log('Order ' + order.id + ' saved to database');
    }
    catch (err) {
        if (err instanceof Sequelize.UniqueConstraintError && err.errors[0].path === 'unique-order-blockchain-id') {
            console.log('[!] Order ' + orderObject.id + ' already exists.');
        }
        else {
            console.log(err);
        }
        return;
    }
}

const applyBid = async (bidObject) => {
    const addr = await models.Addresses.findOne({
        where: {
            hash: bidObject.sender
        }
    });
    if (addr == null) {
        console.log('[!] User with address ' + bidObject.sender + ' does not exist in the database!');
        return;
    }
    const order = await models.Orders.findOne({
        where: {
            blockchain_id: bidObject.orderID
        }
    });
    if (order == null) {
        console.log('[!] Order with ID ' + bidObject.orderID + ' does not exist in the database!');
        return;
    }
    try {
        const bid = await models.Bids.findOne({
            where: {
                tx_id: bidObject.tx_id
            }
        });

        if (bid == null) {
            const newBid = await models.Bids.create({
                order_id: order.id,
                amount: bidObject.amount,
                address_id: addr.id,
                tx_id: bidObject.tx_id,
                status: 'confirmed',
            });

            console.log('Bid ' + newBid.id + ' saved to database');
        } else {
            const [affectedRows] = await models.Bids.update({
                status: 'confirmed',
            }, {
                where: {
                    tx_id: bidObject.tx_id,
                }
            });

            if (affectedRows > 0) {
                console.log('Bid ' + bid.id + ' status updated to CONFIRMED');
            } else {
                console.log('[!] Bid with ID ' + bid.id + ' failed to update');
            }
        }
    }
    catch (err) {
        if (err instanceof Sequelize.UniqueConstraintError && err.errors[0].path === 'unique-bid-tx-id') {
            console.log('[!] Bid with tx hash ' + bidObject.tx_id + ' already exists.');
        }
        else {
            console.log(err);
        }
    }
}

const addBid = async (res) => {
    const bidObject = {
        orderID: res.args.orderID.toString(10),
        amount: res.args.amount.toString(10),
        overbid: res.args.amount.toString(10),
        sender: res.args.senderAddress,
        tx_id: res.transactionHash,
    }
    await applyBid(bidObject);
}

const addBidWithdrawal = async (res) => {
    const bidObject = {
        orderID: res.args.orderID.toString(10),
        amount: -1 * res.args.amount.toString(10),
        sender: res.args.senderAddress,
        tx_id: res.transactionHash,
    }
    await applyBid(bidObject);
}

module.exports = {
    addOrder,
    addBid,
    addBidWithdrawal,
    sequelizeObject: models.sequelize,
};
