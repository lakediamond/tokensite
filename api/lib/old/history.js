const blockchainHandler = require('./blockchain_handler'),
    dbHandler = require('./db_handler');

const START_BLOCK = 3106729;
const END_BLOCK = 'latest';

blockchainHandler.TMTContract.OrderCreated({}, {fromBlock: START_BLOCK, toBlock: END_BLOCK}).get(async (err, res) => {
    if (!err) {
        for (let i=0; i < res.length; i++) {
            await dbHandler.addOrder(res[i]);
        }

        blockchainHandler.TMTContract.BidSubmitted({}, {fromBlock: START_BLOCK, toBlock: END_BLOCK}).get(async (err, res) => {
            if (!err) {
                for (let i=0; i < res.length; i++) {
                    await dbHandler.addBid(res[i]);
                }

                blockchainHandler.TMTContract.BidWithdrawal({}, {fromBlock: START_BLOCK, toBlock: END_BLOCK}).get(async (err, res) => {
                    if (!err) {
                        for (let i=0; i < res.length; i++) {
                            await dbHandler.addBidWithdrawal(res[i]);
                        }
                        dbHandler.sequelizeObject.close();
                    }
                    else {
                        throw err;
                    }
                });

            }
            else {
                throw err;
            }
        });

    }
    else {
        throw err;
    }
});
