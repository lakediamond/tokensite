const blockchainHandler = require('./blockchain_handler'),
    dbHandler = require('./db_handler');

let orderFilter, bidFilter, withdrawalFilter;


function createOrderFilter() {
    console.log('[*] Starting order listener');
    orderFilter = blockchainHandler.TMTContract.OrderCreated();
    orderFilter.watch(async (err, res) => {
        console.log('[*] Received new order event');
        if (!err) {
            dbHandler.addOrder(res);
        }
        else {
            if (err.message === 'filter not found') {
                console.log('[!] Order filter was deleted, recreating it...');
                orderFilter.stopWatching();
                createOrderFilter();
            }
            else {
                console.log('[!] Unknown error in order filter!');
                console.log(err.message);
            }
        }
    });
}

function createBidFilter() {
    console.log('[*] Starting bid listener');
    bidFilter = blockchainHandler.TMTContract.BidSubmitted();
    bidFilter.watch(async (err, res) => {
        console.log('[*] Received new bid event');
        if (!err) {
            dbHandler.addBid(res);
        }
        else {
            if (err.message === 'filter not found') {
                console.log('[!] Bid filter was deleted, recreating it...');
                bidFilter.stopWatching();
                createBidFilter();
            }
            else {
                console.log('[!] Unknown error in bid filter!');
                console.log(err.message);
            }
        }
    });
}

function createWithdrawalFilter() {
    console.log('[*] Starting withdrawal listener');
    withdrawalFilter = blockchainHandler.TMTContract.BidWithdrawal();
    withdrawalFilter.watch(async (err, res) => {
        console.log('[*] Received new withdrawal event');
        if (!err) {
            dbHandler.addBidWithdrawal(res);
        }
        else {
            if (err.message === 'filter not found') {
                console.log('[!] Withdrawal filter was deleted, recreating it...');
                withdrawalFilter.stopWatching();
                createWithdrawalFilter();
            }
            else {
                console.log('[!] Unknown error in withdrawal filter!');
                console.log(err.message);
            }
        }
    });
}

function createOrderCompletionFilter() {
    console.log('[*] Starting OrderIsComplete listener');
    orderFilter = blockchainHandler.TMTContract.OrderIsComplete();
    orderFilter.watch(async (err, res) => {
        console.log('[*] Received new OrderIsComplete event');
        if (!err) {
            // dbHandler.addBidWithdrawal(res);
            console.log(res);
        }
        else {
            if (err.message === 'filter not found') {
                console.log('[!] Withdrawal filter was deleted, recreating it...');
                orderFilter.stopWatching();
                createOrderCompletionFilter();
            }
            else {
                console.log('[!] Unknown error in OrderIsComplete filter!');
                console.log(err.message);
            }
        }
    });
}

createOrderFilter();
createBidFilter();
createWithdrawalFilter();
createOrderCompletionFilter();
