const models = require('../models');

const addresses = {
    'themicp@gmail.com': [
    ],
    'dimit.karakostas@gmail.com': [
    ]
};

(async () => {
    for (email in addresses) {
        let user = await models.User.find({
            where: {email}
        });

        for (hash of addresses[email]) {
            await models.Addresses.create({
                hash,
                user_id: user.id
            });
        }
    }

    models.sequelize.close();
})();
