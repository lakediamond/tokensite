const express = require('express');
const sequelize = require('sequelize');

const models = require('../models');

const router = express.Router();

function normalizeResponse(bids) {
    return bids.map(item => ({
        order_id: +item.order_id,
        order_blockchain_id: +item['Order.blockchain_id'],
        amount: +item.amount,
        user_id: +item['Address.user_id'],
        createdAt: item.createdAt || null,
        active: false, // TODO
        status: item.status,
        tx_id: item.tx_id,
    }));
}

const HOUR_TO_MS = 60 * 60 * 1000;

async function fetchAggregatedBids() {
    return await models.Bids.findAll({
        attributes: [
            'order_id',
            [sequelize.fn('SUM', sequelize.col('amount')), 'amount']
        ],
        include: [
            {
                model: models.Addresses,
                attributes: ['user_id'],
            },
            {
                model: models.Orders,
                attributes: ['blockchain_id', 'expiration'],
                include: {
                    model: models.Types,
                    attributes: ['tmt_needed']
                }
            }
        ],
        group: ['Bids.order_id', 'Address.user_id'],
        where: {
            status: 'confirmed'
        },
        raw: true
    });
}

async function fetchBids(status) {
    return await models.Bids.findAll({
        attributes: [
            'order_id',
            'amount',
            'status',
            'tx_id',
            'createdAt'
        ],
        include: [
            {
                model: models.Addresses,
                attributes: ['user_id'],
            },
            {
                model: models.Orders,
                attributes: ['blockchain_id', 'expiration'],
                include: {
                    model: models.Types,
                    attributes: ['tmt_needed']
                }
            }
        ],
        where: {status},
        raw: true
    });

    return bids;
}

router.get('/', async (req, res) => {
    // typecast as int and then as boolean (because !!'0' == true)
    const active = !!parseInt(req.query.active, 2);
    const aggregate = !!parseInt(req.query.aggregate, 2);
    const status = req.query.status || 'confirmed'; // only used for non aggregated queries

    let bids;
    if (aggregate) {
        bids = await fetchAggregatedBids();
    } else {
        bids = await fetchBids(status);
    }

    let allBids = [];
    const currentDate = new Date().getTime();

    const filled = {}; // aggregate amounts for all orders
    for (let i = 0; i < bids.length; i++) {
        if (!filled[bids[i].order_id]) {
            filled[bids[i].order_id] = 0;
        }

        filled[bids[i].order_id] += +bids[i].amount
    }

    for (let i=0; i < bids.length; i++) { // determine active bids on open orders
        const order_id = bids[i].order_id;

        if (active) {
            if ((!aggregate || bids[i].amount > 0) &&
                    (bids[i]['Order.Type.tmt_needed'] > filled[order_id]) &&
                    (bids[i]['Order.expiration'] >= currentDate + 6*HOUR_TO_MS)) {
                // except for checking if the order is active, also check if
                // in case of aggregated bids, the sum is > 0. Otherwise it
                // means that no active bid is placed (everything has been withdrawn)
                allBids.push(bids[i]);
            }
        }
        else {
            allBids.push(bids[i]);
        }
    }

    res.json(normalizeResponse(allBids));
})

router.post('/', async (req, res) => {
    const bid = req.body;

    try {
        const address = await models.Addresses.findOne({
            attributes: ['id'],
            where: {
                hash: bid.address
            }
        });

        const order = await models.Orders.findOne({
            attributes: ['id'],
            where: {
                blockchain_id: bid.blockchainOrderId
            }
        })

        if (address === null || order === null) {
            return res.status(500).send('Invalid address or order.');
        }

        await models.Bids.create({
            order_id: order.id,
            amount: bid.amount,
            address_id: address.id,
            tx_id: bid.txId,
            status: 'pending',
        });

        res.status(204).send();
    } catch(e) {
        console.log(e);
        res.status(500).send('Failed saving pending bid');
    }
})

router.delete('/', (req, res) => {
    res.send('delete');
})

router.put('/', (req, res) => {
    res.send('put');
});

module.exports = router;
