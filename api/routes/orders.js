const express = require('express');

const models = require('../models');

const router = express.Router();

const HOUR_TO_MS = 60 * 60 * 1000;
const ETH_TO_CHF = 195;

function normalizeResponse(orders) {
    return orders.map(item => ({
        id: item.id,
        blockchain_id: item.blockchain_id,
        type: item.Type.name,
        filled: item.filled,
        filledPercentage: ((item.filled / item.Type.tmt_needed) * 100).toFixed(2),
        tmt_needed: item.Type.tmt_needed - item.filled,
        order_price: (item.conversion * item.Type.tmt_needed).toFixed(3),
        conversion: (item.conversion * ETH_TO_CHF).toFixed(3),
        date: item.createdAt,
        expiration: item.expiration,
    }));
}

router.get('/', async (req, res) => {
    const active = !!parseInt(req.query.active, 2);
    const currentDate = new Date().getTime();

    let orders = await models.Orders.findAll({include: ['Bids', 'Type']});
    let orderBids;
    let resOrders = [];
    // console.log("here1")
    // console.log(orders);
    // console.log("here2")
    for (let i=0; i < orders.length; i++) {
        orderBids = orders[i].Bids;
        orders[i].filled = orderBids.reduce((a, b) => a + b.amount, 0)
        if (orders[i].filled > orders[i].Type.tmt_needed) {
            console.log('Order ' + orders[i]['id'] + ' has been overfilled: ' + orders[i].filled);
            continue;
        }
        if (active && (
            (orders[i].filled >= orders[i].Type.tmt_needed) ||
            ((orders[i]['expiration'] - 6 * HOUR_TO_MS) < currentDate)
        )) {
            continue;
        }
        resOrders.push(orders[i]);
    }

    res.json(normalizeResponse(resOrders));
})

router.post('/', (req, res) => {
    res.send('post');
})

router.delete('/', (req, res) => {
    res.send('delete');
})

router.put('/', (req, res) => {
    res.send('put');
});

module.exports = router;
