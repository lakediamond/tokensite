const express = require('express');

const models = require('../models');

const router = express.Router();

function normalizeResponse(type) {
    return {
        id: type.id,
        name: type.name,
        label: type.label,
    };
}

router.get('/', async (_, res) => {
    const types = await models.Types.findAll();

    res.json(types.map(item => normalizeResponse(item)));
});

module.exports = router;
