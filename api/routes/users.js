const express = require('express');
const sequelize = require('sequelize');

const models = require('../models');

const router = express.Router();

function normalizeResponse(user) {
    return {
        id: user.id,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        addressList: user.Addresses.map(addr => addr.hash),
    };
}

router.post('/register', async (req, res) => {
    const data = req.body;

    try {
        const user = await models.User.create({
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: data.password,
        });

        const address = await models.Addresses.create({
            hash: data.address,
            user_id: user.id,
        });

        res.status(204).send();
    } catch (e) {
        console.log(e);
        // TODO: Match errors and reply with message
        res.status(500).send();
    }
});

router.get('/account', async (req, res) => {
    const email = req.query.email;

    const user = await models.User.find({
        where: {email},
        include: ['Addresses'],
    });

    if (!user) {
        res.status(401).send();
    } else {
        res.json(normalizeResponse(user));
    }
});

module.exports = router;
