const express = require('express');

const models = require('../models');

const router = express.Router();

router.get('/', async (req, res) => {
    let queuedbids = await models.QueuedBids.findAll();
    queuedbids = queuedbids.map(item => ({
        id: item.id,
        amount: item.amount,
        trust: item.trust,
        status: item.status,
        createdAt: item.createdAt,
    }));

    res.json(queuedbids);
})

router.post('/', (req, res) => {
    res.send('post');
})

router.delete('/', (req, res) => {
    res.send('delete');
})

router.put('/', (req, res) => {
    res.send('put');
});

module.exports = router;
