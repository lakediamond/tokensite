const express = require('express');

const models = require('../models');

const router = express.Router();

router.get('/', async (req, res) => {
    const user_id = req.query.user_id;

    if (!user_id) {
        // TODO: HTTP Error code
        return res.json({});
    }

    let user = await models.User.find({
        where: {
            id: user_id,
        }
    });

    let addresses = await user.getAddresses();
    res.json(addresses);
});

module.exports = router;
