require('dotenv').config();

const Sequelize = require('sequelize');
const env = process.env.NODE_ENV || 'development';
const config = require('./config/config')[env];

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const orders = require('./routes/orders');
const queuedbids = require('./routes/queuedbids');
const bids = require('./routes/bids');
const addresses = require('./routes/addresses');
const users = require('./routes/users');
const types = require('./routes/types');

const app = express();
const port = process.env.PORT || 8080;

require('./lib/listener');

var sequelize = new Sequelize(
    process.env.DB_NAME,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    config
);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use('/orders', orders);
app.use('/queuedbids', queuedbids);
app.use('/bids', bids);
app.use('/addresses', addresses);
app.use('/users', users);
app.use('/types', types);

app.get('/health', (_, res) => res.status(204).send());

app.listen(port, () => {
    console.log(`Server up and running in port ${port}.`);
});
