'use strict';
module.exports = (sequelize, DataTypes) => {
    var Addresses = sequelize.define('Addresses', {
        hash: {
            type: DataTypes.STRING,
            unique: true
        },
        user_id: DataTypes.INTEGER
    }, {});
    Addresses.associate = function(models) {
        // associations can be defined here
        Addresses.belongsTo(models.User, {foreignKey: 'user_id', targetKey: 'id'});
        Addresses.hasMany(models.Bids, {foreignKey: 'address_id', targetKey: 'id'});
    };
    return Addresses;
};
