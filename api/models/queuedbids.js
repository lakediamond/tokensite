'use strict';
module.exports = (sequelize, DataTypes) => {
    var QueuedBids = sequelize.define('QueuedBids', {
    amount: DataTypes.INTEGER,
    trust: DataTypes.ENUM('sufficient', 'perfect'),
    status: DataTypes.ENUM('active', 'pending', 'closed')
    }, {});
    QueuedBids.associate = function(models) {
    // associations can be defined here
    };
    return QueuedBids;
};
