'use strict';
module.exports = (sequelize, DataTypes) => {
    var Bids = sequelize.define('Bids', {
        order_id: DataTypes.INTEGER,
        amount: DataTypes.INTEGER,
        address_id: DataTypes.INTEGER,
        status: DataTypes.STRING,
        tx_id: {
            type: DataTypes.STRING,
            unique: true
        },
    }, {});
    Bids.associate = function(models) {
        // associations can be defined here
        Bids.belongsTo(models.Orders, {foreignKey: 'order_id', targetKey: 'id'});
        Bids.belongsTo(models.Addresses, {foreignKey: 'address_id', targetKey: 'id'});
    };
    return Bids;
};
