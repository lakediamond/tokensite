'use strict';
module.exports = (sequelize, DataTypes) => {
    var Types = sequelize.define('Types', {
        name: DataTypes.STRING,
        label: DataTypes.STRING,
        tmt_needed: DataTypes.INTEGER
    }, {});
    Types.associate = function(models) {
        // associations can be defined here
        Types.hasMany(models.Orders, {foreignKey: 'type_id', sourceKey: 'id'});
    };
    return Types;
};
