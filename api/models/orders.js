'use strict';
module.exports = (sequelize, DataTypes) => {
    var Orders = sequelize.define('Orders', {
        type_id: DataTypes.INTEGER,
        blockchain_id: {
            type: DataTypes.INTEGER,
            unique: true
        },
        expiration: DataTypes.DATE,
        conversion: DataTypes.DOUBLE,
        metadata: DataTypes.STRING
    }, {});
    Orders.associate = function(models) {
        // associations can be defined here
        Orders.belongsTo(models.Types, {foreignKey: 'type_id', targetKey: 'id'});
        Orders.hasMany(models.Bids, {foreignKey: 'order_id', sourceKey: 'id'});
    };
    return Orders;
};
