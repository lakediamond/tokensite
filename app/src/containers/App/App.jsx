import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from 'react-redux'
// creates a beautiful scrollbar
import PerfectScrollbar from "perfect-scrollbar";
import "perfect-scrollbar/css/perfect-scrollbar.css";
import { withStyles } from "material-ui";

import { Header, Sidebar } from "components";
import Snackbar from 'material-ui/Snackbar';
import IconButton from 'material-ui/IconButton';
import { Close } from 'material-ui-icons';

import appRoutes from "routes/app.jsx";

import Login from "views/Login/Login.jsx";
import Register from "views/Register/Register.jsx";

import appStyle from "variables/styles/appStyle.jsx";

import image from "assets/img/quantum-bg.jpg";
import logo from "assets/img/ld_logo.png";

import {
    closeAlert,
    showAlert
} from '../../actions';

const switchRoutes = (
    <Switch>
        {appRoutes.map((prop, key) => {
            if (prop.redirect)
                return <Redirect from={prop.path} to={prop.to} key={key} />;
            return <Route path={prop.path} component={prop.component} key={key} />;
        })}
    </Switch>
);

class App extends React.Component {
    state = {
        mobileOpen: false,
    };

    handleDrawerToggle = () => {
        this.setState({ mobileOpen: !this.state.mobileOpen });
    };
    componentDidMount() {
        if(navigator.platform.indexOf('Win') > -1){
            // eslint-disable-next-line
            const ps = new PerfectScrollbar(this.refs.mainPanel);
        }

        const loginPath = this.props.location.pathname === '/login';
        const registerPath = this.props.location.pathname === '/register';

        if (!localStorage.getItem('user') && (!loginPath && !registerPath)) {
                this.props.history.push('/login');
        }

    }
    componentDidUpdate() {
        if (this.refs.mainPanel) {
            this.refs.mainPanel.scrollTop = 0;
        }
    }
    render() {
        const alertPosition = {
            vertical: 'top',
            horizontal: 'center'
        };
        const { classes, ...rest } = this.props;
        const loginPath = this.props.location.pathname === '/login';
        const registerPath = this.props.location.pathname === '/register';

        return (
            loginPath || registerPath ? (
                <div className={classes.wrapper}>
                    {loginPath ? (
                        <Login history={rest.history} />
                    ) : (
                        <Register history={rest.history} />
                    )}
                </div>
            ) : (
                <div className={classes.wrapper}>
                    <Sidebar
                        routes={appRoutes}
                        logoText={"Lake Diamond"}
                        logo={logo}
                        image={image}
                        handleDrawerToggle={this.handleDrawerToggle}
                        open={this.state.mobileOpen}
                        color="blue"
                        {...rest}
                    />
                    <div className={classes.mainPanel} ref="mainPanel">
                        <Header
                            routes={appRoutes}
                            handleDrawerToggle={this.handleDrawerToggle}
                            {...rest}
                        />
                        <div className={classes.content}>
                            <div className={classes.container}>{switchRoutes}</div>
                        </div>
                    </div>
                    <Snackbar
                        anchorOrigin={alertPosition}
                        open={this.props.alertMessage.open}
                        onClose={this.props.closeAlert}
                        message={this.props.alertMessage.message}
                        action={[
                            <IconButton
                                key="close"
                                aria-label="Close"
                                color="inherit"
                                className={classes.close}
                                onClick={this.props.closeAlert}
                            >
                                <Close />
                            </IconButton>
                        ]}
                    />
                </div>
            )
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        alertMessage: state.alertMessage
    };
};

const mapDispatchToProps = dispatch => {
    return {
        closeAlert: () => {
            dispatch(closeAlert());
        },
        showAlert: message => {
            dispatch(showAlert(message));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(appStyle)(App));
