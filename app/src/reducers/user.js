import config from '../config.json';

const defaultState = JSON.parse(localStorage.getItem('user')) || {
    id: config.user,
    email: '',
    firstName: '',
    lastName: '',
    addressList: []
};

const user = (state = defaultState, action) => {
    switch (action.type) {
        case 'LOGIN_USER':
            return action.data;
        default:
            return state;
    }
};

export default user;
