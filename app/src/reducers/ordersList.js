const ordersList = (state = [], action) => {
    switch (action.type) {
        case 'RECEIVE_ORDERS_LIST':
            if (JSON.stringify(action.data) === JSON.stringify(state)) {
                return state;
            }
            return action.data;
        default:
            return state;
    }
};

export default ordersList;
