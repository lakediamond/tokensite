const myBidsNumber = (state = 0, action) => {
    switch (action.type) {
        case 'RECEIVE_MY_BIDS_NUMBER':
            if (action.data === state) {
                return state;
            }
            return action.data;
        default:
            return state;
    }
};

export default myBidsNumber;
