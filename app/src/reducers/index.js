import { combineReducers } from 'redux';
import ordersList from './ordersList';
import bidsList from './bidsList';
import queuedBidsList from './queuedBidsList';
import myBidsNumber from './myBidsNumber';
import alertMessage from './alertMessage';
import user from './user';

const tokensiteApp = combineReducers({
    ordersList,
    bidsList,
    queuedBidsList,
    myBidsNumber,
    alertMessage,
    user,
});

export default tokensiteApp;
