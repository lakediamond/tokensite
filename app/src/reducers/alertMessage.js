const defaultState = {
    open: false,
    message: '',
};

const alertMessage = (state = defaultState, action) => {
    switch (action.type) {
        case 'CLOSE_ALERT':
            return Object.assign({}, state, {open: false});
        case 'SHOW_ALERT':
            return Object.assign({}, state, {open: true, message: action.message});
        default:
            return state;
    }
};

export default alertMessage;
