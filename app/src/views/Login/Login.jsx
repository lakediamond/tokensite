import React from "react";
import { connect } from 'react-redux'
import PropTypes from "prop-types";

import { Button } from "components";
import TextField from 'material-ui/TextField';
import axios from '../../lib/axiosWrapper';
import P from '../../components//Typography/P.jsx';
import Card, { CardActions, CardContent } from 'material-ui/Card';

import {
    loginUser,
} from '../../actions';

import logo from "assets/img/ld_logo.png";

import { withStyles } from "material-ui";
import { infoColor } from "../../variables/styles";

const styles = {
    card: {
        width: 350,
        maxWidth: '80%',
        margin: 'auto'
    },
    header: {
        textAlign: 'center',
        '& img': {
            height: 45,
            verticalAlign: 'middle',
        }
    },
    a: {
        color: infoColor,
        textDecoration: "none",
        backgroundColor: "transparent"
    },
    links: {
        width: 350,
        maxWidth: '80%',
        margin: '20px auto 50px auto',
    }
};

class Login extends React.Component {
    state = {
        email: ''
    }

    handleSubmit = async () => {
        // TODO: only for the demo

        try {
            const { data } = await axios.get('/users/account', {
                params: {
                    email: this.state.email
                }
            });

            this.props.loginUser(data);
            this.props.history.push('/');
        } catch (e) {
            alert('User not found');
        }
    }

    handleInputChange = (inputName, event) => {
        if (inputName === 'email') {
            this.setState({email: event.target.value});
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <h2 className={classes.header}><img alt='logo' src={logo} /> Lake Diamond</h2>
                <Card className={classes.card}>
                    <CardContent>
                        <TextField
                            id="email-input"
                            label="Email"
                            className={classes.textField}
                            margin="normal"
                            onChange={this.handleInputChange.bind(this, 'email')}
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />

                        <TextField
                            id="password-input"
                            label="Password"
                            className={classes.textField}
                            type="password"
                            margin="normal"
                            onChange={this.handleInputChange.bind(this, 'password')}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />
                    </CardContent>
                    <CardActions>
                        <Button
                                type='button'
                                color="primary"
                                fullWidth
                                onClick={this.handleSubmit}
                                className={classes.button}>
                                Login
                        </Button>
                    </CardActions>
                </Card>
                <div className={classes.links}>
                    <P><a className={classes.a} href=''>Forgot your password??</a></P>
                    <P>Don't have an account? <a className={classes.a} href='/register'>Sign up</a> here</P>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapDispatchToProps = dispatch => {
    return {
        loginUser: user => {
            dispatch(loginUser(user));
        },
    };
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(Login));
