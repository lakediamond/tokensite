import React from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux'

import { withStyles } from "material-ui";
import dashboardStyle from "variables/styles/dashboardStyle";

import Grid from 'material-ui/Grid';

import {
    ItemGrid,
    RegularCard,
    SortableTable,
    OrdersTable,
} from "components";

import { fetchOrdersList, fetchBidsList } from '../../actions';

import { tableColumns } from "variables/tables";

function cloneArray(array) {
    return JSON.parse(JSON.stringify(array));
}

class History extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            bids: {
                order: 'desc',
                orderBy: 'createdAt',
            },
            myBids: {
                order: 'desc',
                orderBy: 'createdAt',
            }
        };
    }

    fetchTables = () => {
        this.props.fetchOrdersList({active: false});
        this.props.fetchBidsList({active: false, aggregate: false});
        this.timeouts.push(setTimeout(this.fetchTables, 1000));
    }

    componentWillMount() {
            this.timeouts = [];
    }

    componentDidMount() {
        this.fetchTables();
    }

    componentWillUnmount() {
        this.timeouts.forEach(clearTimeout);
    }

    handleSortChange = (table, order, orderBy) => {
        const state = this.state;
        state[table].orderBy = orderBy;
        state[table].order = order;
        this.setState(state);
    }

    render() {
        const bidsList = cloneArray(this.props.bidsList);
        const myBidsList = cloneArray(this.props.bidsList).filter(item => this.props.user.id === item.user_id);
        if (this.state.bids.order === 'asc') {
            bidsList.sort((a, b) => (a[this.state.bids.orderBy] < b[this.state.bids.orderBy] ?  -1 : 1));
        } else {
            bidsList.sort((a, b) => (a[this.state.bids.orderBy] < b[this.state.bids.orderBy] ?  1 : -1));
        }
        if (this.state.myBids.order === 'asc') {
            myBidsList.sort((a, b) => (a[this.state.myBids.orderBy] < b[this.state.myBids.orderBy] ?  -1 : 1));
        } else {
            myBidsList.sort((a, b) => (a[this.state.myBids.orderBy] < b[this.state.myBids.orderBy] ?  1 : -1));
        }

        return (
            <div>
                <Grid container>
                    <ItemGrid xs={12} sm={12} md={6}>
                        <RegularCard
                            headerColor="orange"
                            cardTitle='My Activity'
                            content={
                                <SortableTable
                                    data={myBidsList}
                                    minWidth={false}
                                    columns={tableColumns['History Bids']}
                                    handleSortChange={this.handleSortChange.bind(this, 'myBids')} />
                            }
                        />
                    </ItemGrid>
                    <ItemGrid xs={12} sm={12} md={6}>
                        <RegularCard
                            headerColor="orange"
                            cardTitle='All Proposals'
                            content={
                                <SortableTable
                                    data={bidsList}
                                    columns={tableColumns['History Bids']}
                                    minWidth={false}
                                    handleSortChange={this.handleSortChange.bind(this, 'bids')} />
                            }
                        />
                    </ItemGrid>
                    <ItemGrid xs={12} sm={12} md={12}>
                        <OrdersTable
                            cardTitle='Orders'
                            actions={false}
                            data={this.props.ordersList}
                        />
                    </ItemGrid>
                </Grid>
            </div>
        );
    }
}

History.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        ordersList: state.ordersList,
        bidsList: state.bidsList
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchOrdersList: ({active}) => {
            dispatch(fetchOrdersList({active}));
        },
        fetchBidsList: ({active, aggregate}) => {
            dispatch(fetchBidsList({active, aggregate}));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(dashboardStyle)(History));
