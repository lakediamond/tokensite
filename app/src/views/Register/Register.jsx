import React from "react";
import PropTypes from "prop-types";
import axios from '../../lib/axiosWrapper';

import { Button } from "components";
import TextField from 'material-ui/TextField';
import P from '../../components//Typography/P.jsx';
import Card, { CardActions, CardContent } from 'material-ui/Card';

import logo from "assets/img/ld_logo.png";

import { withStyles } from "material-ui";
import { infoColor } from "../../variables/styles";

const styles = {
    card: {
        width: 350,
        maxWidth: '80%',
        margin: '0 auto 50px auto'
    },
    header: {
        textAlign: 'center',
        '& img': {
            height: 45,
            verticalAlign: 'middle',
        }
    },
    a: {
        color: infoColor,
        textDecoration: "none",
        backgroundColor: "transparent"
    },
    links: {
        width: 350,
        maxWidth: '80%',
        margin: '20px auto 50px auto',
    }
};

class Register extends React.Component {
    state = {
        form: {
            firstName: '' ,
            lastName: '' ,
            email: '',
            address: '',
            password: '',
        }
    }

    handleInputChange = (field, event) => {
        const state = this.state;
        state.form[field] = event.target.value;
        this.setState(state);
    }

    handleSubmit = async () => {
        if (!this.state.form.firstName ||
                !this.state.form.lastName ||
                !this.state.form.email ||
                !this.state.form.address ||
                !this.state.form.password) {
            alert('Please fill in all the fields.');
            return;
        }

        try {
            await axios.post('/users/register', this.state.form);

            this.props.history.push('/login');
        } catch(e) {
            console.log(e);
            throw(e);
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <h2 className={classes.header}><img src={logo} alt={'The Time Machine Tokens logo'} /> Lake Diamond</h2>
                <Card className={classes.card}>
                    <CardContent>
                        <TextField
                            id="firstname-input"
                            label="First Name"
                            className={classes.textField}
                            onChange={this.handleInputChange.bind(this, 'firstName')}
                            placeholder='John'
                            margin="normal"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />

                        <TextField
                            id="lastname-input"
                            label="Last Name"
                            className={classes.textField}
                            onChange={this.handleInputChange.bind(this, 'lastName')}
                            placeholder='Doe'
                            margin="normal"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />

                        <TextField
                            id="email-input"
                            label="E-Mail"
                            className={classes.textField}
                            onChange={this.handleInputChange.bind(this, 'email')}
                            placeholder='test@example.com'
                            margin="normal"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />

                        <TextField
                            id="address"
                            label="Account Address"
                            className={classes.textField}
                            onChange={this.handleInputChange.bind(this, 'address')}
                            placeholder='0x0000000000000000000000000000000000000000'
                            margin="normal"
                            fullWidth
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />

                        <TextField
                            id="password-input"
                            label="Password"
                            className={classes.textField}
                            onChange={this.handleInputChange.bind(this, 'password')}
                            type="password"
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />

                        <TextField
                            id="password-confirm-input"
                            label="Confirm Password"
                            className={classes.textField}
                            type="password"
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            fullWidth
                        />
                    </CardContent>
                    <CardActions>
                        <Button
                            type='button'
                            color="primary"
                            fullWidth
                            className={classes.button}
                            onClick={this.handleSubmit}
                            >
                                Register
                        </Button>
                    </CardActions>
                </Card>
                <div className={classes.links}>
                    <P>Already have an account? <a className={classes.a} href='/login'>Login here</a></P>
                </div>
            </div>
        );
    }
}

Register.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Register);
