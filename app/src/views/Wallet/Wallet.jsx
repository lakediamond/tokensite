import React from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux'

import { withStyles, Grid } from "material-ui";
import dashboardStyle from "variables/styles/dashboardStyle";
import {
    LocalAtm,
    AccessTime,
} from "material-ui-icons";

import {
    StatsCard,
    ItemGrid,
    ChartCard,
} from "components";

import ChartistGraph from "react-chartist";

import {
    dailySalesChart,
} from "variables/charts";

import { checkBalance } from '../../actions';

class Wallet extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            EthTmtTrade: 0.0027,
            EurTmtTrade: 0.48,
            balance: 0,
            web3Account: ''
        };
    }

    fetchBalance = async () => {
        try {
            const balanceResponse = await this.props.checkBalance();
            if (this.state.balance !== balanceResponse) {
                this.setState({balance: balanceResponse});
            }
        } catch (e) {
            this.setState({balance: 0});
        }

        this.timeouts.push(setTimeout(this.fetchBalance, 5000));
    }

    componentWillMount() {
        this.timeouts = [];
    }

    componentDidMount() {
        this.fetchBalance();
        this.getWeb3Account();
    }

    componentDidUpdate(prevProps, prevState){
        if(this.state.web3Account != prevState.web3Account) {
            this.getETHBalance();
        }
    }

    componentWillUnmount() {
        this.timeouts.forEach(clearTimeout);
    }

    getTMTPriceData = () => {
        const currentDate = new Date();

        if (!('dayLabels' in this.state)) {
                const days = ["M", "T", "W", "T", "F", "S", "S"];
                const currentDay = currentDate.getDay();
                let labels = [];
                for (let i=currentDay; i < currentDay + 7; i++) {
                    labels.push(days[i % 7]);
                }
                this.setState({dayLabels: labels});
        }

        let weeklyPrices = [1.5, 1.9, 2, 1.7, 2.1, 2.5, 2.98];

        let options = dailySalesChart.options;
        const minPrice = Math.min.apply(Math, weeklyPrices);
        const maxPrice = Math.max.apply(Math, weeklyPrices);
        options.low = minPrice - (0.1 * minPrice);
        options.high = maxPrice + (0.1 * maxPrice);

        return {
         'data': {
             labels: this.state.dayLabels,
             series: [weeklyPrices]
         },
         'options': options
        }
    }

    getWeb3Account = () => {
        const that = this;
        window.web3.eth.getAccounts(function(error, result) {
            that.setState({"web3Account": result[0]})
        })
    }

    getETHBalance = () => {
        const that = this;
        const {web3Account} = that.state;
        window.web3.eth.getBalance(web3Account, function(error, result) {
            if (!error) {
                const res = JSON.stringify(result).replace(/"/g, "");
                const balance = Number(res);
                const ETHBalance = window.web3.fromWei(balance, 'ether');
                that.setState({"ethBalance": ETHBalance});
            } else {
                console.error(error);
            }
        });
    }

    render() {
        return (
                <div>
                    <Grid container>
                        <ItemGrid xs={12} sm={6} md={3}>
                            <StatsCard
                                icon={LocalAtm}
                                iconColor="green"
                                title="Balance"
                                description={this.state.balance + ' LKD'}
                                statIcon={LocalAtm}
                                statText="Wallet"
                            />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                            <StatsCard
                                icon={LocalAtm}
                                iconColor="blue"
                                title="Balance"
                                description={this.state.ethBalance? Number(this.state.ethBalance).toFixed(3) + ' ETH': 0}
                                statIcon={LocalAtm}
                                statText={'1 LKD = ' + this.state.EthTmtTrade + ' ETH'}
                            />
                        </ItemGrid>
                        <ItemGrid xs={12} sm={6} md={3}>
                            <StatsCard
                                icon={LocalAtm}
                                iconColor="purple"
                                title="Balance"
                                description={(this.state.balance * this.state.EurTmtTrade).toFixed(3) + ' EUR'}
                                statIcon={LocalAtm}
                                statText={'1 LKD = ' + this.state.EurTmtTrade + ' EUR'}
                            />
                        </ItemGrid>
                    </Grid>
                    <Grid container>
                        <ItemGrid xs={12} sm={12} md={12}>
                            <a href="https://coinmarketcap.com" target="_blank" rel="noopener noreferrer">
                            <ChartCard
                                chart={
                                    <ChartistGraph
                                        className="ct-chart"
                                        data={this.getTMTPriceData()['data']}
                                        type="Line"
                                        options={this.getTMTPriceData()['options']}
                                        listener={dailySalesChart.animation}
                                    />
                                }
                                chartColor="blue"
                                title="LKD weekly price (EUR)"
                                text=""
                                statIcon={AccessTime}
                                statText=""
                            />
                            </a>
                        </ItemGrid>
                    </Grid>
                </div>
        );
    }
}

Wallet.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = dispatch => {
    return {
        checkBalance: () => {
            return dispatch(checkBalance());
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(dashboardStyle)(Wallet));
