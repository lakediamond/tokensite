import React from "react";
import Grid from 'material-ui/Grid';
import { connect } from 'react-redux'
import { withStyles } from 'material-ui/styles';
import config from '../../config.json';
import {
    ItemGrid,
    RegularCard,
    SortableTable,
    Button,
} from "components";

import {
    fetchBidsList,
    withdrawBid,
    showAlert
} from '../../actions';
import { tableColumns } from "variables/tables";
import ConfirmationDialog from './ConfirmationDialog';

function cloneArray(array) {
    return JSON.parse(JSON.stringify(array));
}

const styles = theme => ({
    close: {
        width: theme.spacing.unit * 4,
        height: theme.spacing.unit * 4,
    },
});

class Bids extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            myBids: {
                order: 'desc',
                orderBy: 'order_blockchain_id',
            },
            allBids: {
                order: 'desc',
                orderBy: 'order_blockchain_id',
            },
            confirmationDialog: {
                open: false,
                blockchainOrderId: 0,
                maxAmount: 0,
            },
        };
    }

    fetchBids = () => {
        this.props.fetchBidsList({active: true, aggregate: true});

        this.timeouts.push(setTimeout(this.fetchBids, 1000));
    }

    handleWithdrawRequest = (blockchainOrderId, maxAmount) => {
        this.setState({confirmationDialog: {open: true, blockchainOrderId, maxAmount}});
    }

    handleAction = async (isAccepted, blockchainOrderId, withdrawAmount) => {
        let alertMessage = false;
        if (isAccepted) {
            try {
                await this.props.withdrawBid({blockchainOrderId, withdrawAmount});

                alertMessage = `Successful withdrawal. You will be refunded ${withdrawAmount} LKD soon.`;
            } catch (e) {
                alertMessage = e.message;
            }
        }

        const state = this.state;
        state.confirmationDialog.open = false;
        this.setState(state);
        if (alertMessage) {
            this.props.showAlert(alertMessage);
        }
    }

    componentWillMount() {
            this.timeouts = [];
    }

    componentDidMount() {
        this.fetchBids();
    }

    componentWillUnmount() {
        this.timeouts.forEach(clearTimeout);
    }

    handleSortChange = (table, order, orderBy) => {
        const state = this.state;
        state[table] = {orderBy, order};
        this.setState(state);
    }

    render() {
        const allBids = cloneArray(this.props.bidsList)
            .map(item => { // create clone and modify it
                if (item.user_id !== this.props.user.id) {
                    return item;
                }

                item._actionElement =
                    item.status === 'pending' ? (
                        <Button
                            color="white"
                            target='_blank'
                            href={`${config.etherscan_path}/tx/${item.tx_id}`}
                        >
                            Pending
                        </Button>
                    ) : (
                        <Button
                            onClick={this.handleWithdrawRequest.bind(this, item.order_blockchain_id, item.amount)}
                            color="danger"
                        >
                            Withdraw
                        </Button>
                    );

                return item;
            });
        const myBids = allBids.filter(item => this.props.user.id === item.user_id);

        if (this.state.allBids.order === 'asc') {
            allBids.sort((a, b) => (a[this.state.allBids.orderBy] < b[this.state.allBids.orderBy] ?  -1 : 1));
        } else {
            allBids.sort((a, b) => (a[this.state.allBids.orderBy] < b[this.state.allBids.orderBy] ?  1 : -1));
        }
        if (this.state.myBids.order === 'asc') {
            myBids.sort((a, b) => (a[this.state.myBids.orderBy] < b[this.state.myBids.orderBy] ?  -1 : 1));
        } else {
            myBids.sort((a, b) => (a[this.state.myBids.orderBy] < b[this.state.myBids.orderBy] ?  1 : -1));
        }

        return (
        <div>
            <ConfirmationDialog
                onAction={this.handleAction}
                {...this.state.confirmationDialog}
            />

            <Grid container>
                <ItemGrid xs={12} sm={6} md={6}>
                    <RegularCard
                        headerColor="orange"
                        cardTitle="My Proposals"
                        content={
                            <SortableTable
                                data={myBids}
                                minWidth={false}
                                highlight={item => item.status === 'pending'}
                                handleSortChange={this.handleSortChange.bind(this, 'myBids')}
                                columns={tableColumns['Bids']} />
                        }
                    />
                </ItemGrid>
                <ItemGrid xs={12} sm={6} md={6}>
                    <RegularCard
                        headerColor="orange"
                        cardTitle="All Proposals"
                        content={
                            <SortableTable
                                data={allBids}
                                minWidth={false}
                                handleSortChange={this.handleSortChange.bind(this, 'allBids')}
                                columns={tableColumns['Bids']} />
                        }
                    />
                </ItemGrid>
            </Grid>
        </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
        bidsList: state.bidsList
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchBidsList: ({active, aggregate}) => {
            dispatch(fetchBidsList({active, aggregate}));
        },
        showAlert: message => {
            dispatch(showAlert(message));
        },
        withdrawBid: ({blockchainOrderId, withdrawAmount}) => {
            return dispatch(withdrawBid({blockchainOrderId, withdrawAmount}));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Bids));
