import React from "react";
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Dialog, {
    DialogActions,
    DialogContent,
    DialogTitle,
} from 'material-ui/Dialog';
import { Button } from "components";
import Input, { InputAdornment } from 'material-ui/Input';
import { metaMaskLoadingMessage } from 'variables/general';

const styles = theme => ({
    input: {
        marginLeft: '10px'
    },
    loadingMessageWrapper: {
        position: 'relative',
        textAlign: 'center',
        fontWeight: 'bold'
    }
});

class ConfirmationDialog extends React.Component {
    state = {
        amount: 0,
        actionPending: false,
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open) {
            // reset only when opening dialog again
            // because previously right before disappearing
            // the dialog, the user could see the value being resetted
            this.setState({amount: nextProps.maxAmount, actionPending: false});
        }
    }

    handleAction = async accepted => {
        if (accepted) {
            this.setState({actionPending: true});
        }
        await this.props.onAction(accepted, this.props.blockchainOrderId, +this.state.amount);
    }

    handleInputChange = event => {
        this.setState({amount: event.target.value});
    }

    render() {
        const { classes } = this.props;
        return (
            <Dialog
                open={this.props.open}
                maxWidth='xs'
                onClose={this.handleAction.bind(this, false)}
            >
                <DialogTitle id="alert-dialog-slide-title">
                    {`Are you sure you want to withdraw from order ${this.props.blockchainOrderId}?`}
                </DialogTitle>
                <DialogContent>
                    Withdrawal amount:
                    <Input
                        type='number'
                        className={classes.input}
                        onChange={this.handleInputChange}
                        endAdornment={<InputAdornment position="end">LKD</InputAdornment>}
                        inputProps={{
                            min: 0,
                            max: this.props.maxAmount,
                            value: this.state.amount
                        }} />
                </DialogContent>
                {this.state.actionPending ? (
                    <div className={classes.loadingMessageWrapper}>
                        <p>{metaMaskLoadingMessage}</p>
                    </div>
                ) : (
                    <DialogActions>
                        <Button onClick={this.handleAction.bind(this, false)} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleAction.bind(this, true)} color="primary">
                            Withdraw
                        </Button>
                    </DialogActions>
                    )}
            </Dialog>
        );
    }
}

ConfirmationDialog.propTypes = {
    onAction: PropTypes.func.isRequired,
    blockchainOrderId: PropTypes.number.isRequired,
    maxAmount: PropTypes.number.isRequired,
    open: PropTypes.bool.isRequired,
};

export default withStyles(styles)(ConfirmationDialog);
