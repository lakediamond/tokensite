import React from "react";
import Grid from 'material-ui/Grid';
import { connect } from 'react-redux'
import {
    ItemGrid,
    SimpleMenu,
    OrdersTable,
} from "components";
import {
    fetchOrdersList,
} from '../../actions';
import axios from '../../lib/axiosWrapper';

class OrdersList extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            type: 'all', // all, platelet, round brilliant
            types: [],
        }
    }

    fetchOrders = () => {
        this.props.fetchOrdersList({active: true});
        this.timeouts.push(setTimeout(this.fetchOrders, 1000));
    }

    componentWillMount() {
        this.timeouts = [];
    }

    async componentDidMount() {
        this.fetchOrders();

        const { data } = await axios.get('/types');
        this.setState({types: data});
    }

    componentWillUnmount() {
        this.timeouts.forEach(clearTimeout);
    }

    handleOrderTypeChange = (option) => {
        this.setState({type: option});
    }

    render() {
        const options = [
            { label: 'All', name: 'all' },
            ...this.state.types
        ];

        const data = this.props.ordersList.filter(
            item => item.type === this.state.type || this.state.type === 'all'
        );

        return (
          <div>
              <Grid container>
                  <ItemGrid xs={12} sm={12} md={12}>
                      <SimpleMenu options={options} label='Order Type' handleOptionChange={this.handleOrderTypeChange} />

                      <OrdersTable
                          cardTitle="Orders"
                          data={data}
                      />
                  </ItemGrid>
              </Grid>
          </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        ordersList: state.ordersList
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchOrdersList: ({active}) => {
            dispatch(fetchOrdersList({active}));
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(OrdersList);
