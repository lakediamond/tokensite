import React from "react";
import { Grid } from "material-ui";
import Card, { CardContent } from "material-ui/Card";
import { connect } from 'react-redux'

import { Snackbar } from 'components';
import {
    RegularCard,
    Button,
    CustomInput,
    ItemGrid
} from "components";

class Settings extends React.Component {
    constructor(props, context) {
        super(props, context);

        const savedUser = null;
        const defaultUser = {
            userId: '359378',
            name: 'Jim',
            email: 'jim@signedblock.com',
            ethereum: '0x000000000000000000000',
        }

        this.state = {
            ...savedUser || defaultUser,
            newPass: '',
            newPassVerify: '',
            password: '',
            error: false,
            success: false,
        };
    }

    handleNewPass = (id, e) => {
        var value = e.target.value;
        switch (id) {
            case 'name':
                this.setState({name: value});
                break;
            case 'email':
                this.setState({email: value});
                break;
            case 'ethereum':
                this.setState({ethereum: value});
                break;
            case 'new-password':
                this.setState({newPass: value});
                break;
            case 'new-password-verify':
                this.setState({newPassVerify: value});
                break;
            case 'password':
                this.setState({password: value});
                break;
            default:
                break;
        }
    };

    closeError = () => {
        this.setState({error: false});
    }

    closeSuccess = () => {
        this.setState({success: false});
    }

    handleUpdate = () => {
        if (this.state.password === '123456') {
            this.setState({success: true});
            setTimeout(this.closeSuccess, 4000);
        } else {
            this.setState({error: true});
            setTimeout(this.closeError, 4000);
        }
    };

    render() {
        return (
            <div>
                <Grid container>
                    <ItemGrid xs={12} sm={12} md={12}>
                        <RegularCard
                            cardTitle="Edit Profile"
                            content={
                                <div>
                                    <Grid container>
                                        <ItemGrid xs={12} sm={12} md={2}>
                                            <CustomInput
                                                labelText='User ID'
                                                id="user-id"
                                                inputProps={{
                                                    defaultValue: this.props.user.id,
                                                    disabled: true,
                                                }}
                                                formControlProps={{
                                                    fullWidth: true
                                                }}
                                            />
                                        </ItemGrid>
                                    </Grid>
                                    <Grid container>
                                        <ItemGrid xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="Name"
                                                id="name"
                                                inputProps={{
                                                    defaultValue: `${this.props.user.firstName} ${this.props.user.lastName}`,
                                                    disabled: true,
                                                }}
                                                formControlProps={{
                                                    onChange: (e) => {this.handleNewPass('name', e)},
                                                    fullWidth: true
                                                }}
                                            />
                                        </ItemGrid>
                                        <ItemGrid xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="Email address"
                                                id="email-address"
                                                inputProps={{
                                                    defaultValue: this.props.user.email,
                                                    disabled: true,
                                                }}
                                                formControlProps={{
                                                    onChange: (e) => {this.handleNewPass('email', e)},
                                                    fullWidth: true
                                                }}
                                            />
                                        </ItemGrid>
                                    </Grid>
                                    <Grid container>
                                        <ItemGrid xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="ETH address"
                                                id="eth-address"
                                                inputProps={{
                                                    defaultValue: this.props.user.addressList[0],
                                                    disabled: true,
                                                }}
                                                formControlProps={{
                                                    onChange: (e) => {this.handleNewPass('ethereum', e)},
                                                    fullWidth: true
                                                }}
                                            />
                                        </ItemGrid>
                                    </Grid>
                                    <Grid container>
                                        <ItemGrid xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="New Password"
                                                id="new-password"
                                                inputProps={{
                                                    type: 'password'
                                                }}
                                                formControlProps={{
                                                    onChange: (e) => {this.handleNewPass('new-password', e)},
                                                    fullWidth: true
                                                }}
                                            />
                                        </ItemGrid>
                                        <ItemGrid xs={12} sm={12} md={6}>
                                            <CustomInput
                                                labelText="Verify New Password"
                                                id="new-password-verify"
                                                inputProps={{
                                                    type: 'password'
                                                }}
                                                formControlProps={{
                                                    onChange: (e) => {this.handleNewPass('new-password-verify', e)},
                                                    fullWidth: true
                                                }}
                                            />
                                        </ItemGrid>
                                    </Grid>
                                </div>
                            }
                        />
                    </ItemGrid>
                    <ItemGrid xs={12} sm={12} md={12}>
                        <Card>
                            <CardContent>
                                <Grid container>
                                    <ItemGrid xs={12} sm={12} md={6}>
                                        <CustomInput
                                            labelText="Current Password"
                                            id="password"
                                            inputProps={{
                                                    type: 'password'
                                            }}
                                            formControlProps={{
                                                onChange: (e) => {this.handleNewPass('password', e)},
                                                fullWidth: true
                                            }}
                                        />
                                        <Button onClick={this.handleUpdate} color="primary">Update Profile</Button>

                                        <Snackbar place="tc" closeNotification={this.closeError} open={this.state.error} message={'The password was invalid. Try again.'} close color="danger"/>
                                        <Snackbar place="tc" closeNotification={this.closeSuccess} open={this.state.success} message={'Changes saved successfully.'} close color="success"/>
                                    </ItemGrid>
                                </Grid>
                            </CardContent>
                        </Card>
                    </ItemGrid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
    };
};

export default connect(mapStateToProps)(Settings);
