import React from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux'

import {
    LocalAtm,
    TrendingUp,
    AccessTime,
} from "material-ui-icons";
import { withStyles, Grid } from "material-ui";

import {
    StatsCard,
    ItemGrid,
    ChartCard,
    OrdersTable,
} from "components";

import ChartistGraph from "react-chartist";

import {
    fetchOrdersList,
    fetchBidsList,
    checkBalance,
    submitBid,
    showAlert
} from '../../actions';

import {
    dailySalesChart,
} from "variables/charts";

import dashboardStyle from "variables/styles/dashboardStyle";

class Dashboard extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            balance: 0
        };
    }

    fetchBalance = async () => {
        try {
            const balanceResponse = await this.props.checkBalance();
            if (this.state.balance !== balanceResponse) {
                this.setState({balance: balanceResponse});
                this.forceUpdate();
            }
        } catch (e) {
            console.log(e);
        }
    }

    fetchTables = () => {
        this.props.fetchOrdersList({active: true});
        this.props.fetchBidsList({active: true, aggregate: true});
        this.fetchBalance();

        this.timeouts.push(setTimeout(this.fetchTables, 1000));
    }

    componentWillMount() {
        this.timeouts = [];
    }

    componentDidMount() {
        this.fetchTables();
    }

    componentWillUnmount() {
        this.timeouts.forEach(clearTimeout);
    }

    shouldComponentUpdate(nextProps) {
        const differentOrders =
            JSON.stringify(this.props.ordersList) !== JSON.stringify(nextProps.ordersList);
        const differentBidsNumber = this.props.myBidsNumber !== nextProps.myBidsNumber;

        return differentOrders || differentBidsNumber;
    }

    getWeeklyOrderChartData = (orders) => {
        const currentDate = new Date();

        if (!('dayLabels' in this.state)) {
                const days = ["M", "T", "W", "T", "F", "S", "S"];
                const currentDay = currentDate.getDay();
                let labels = [];
                for (let i=currentDay; i < currentDay + 7; i++) {
                    labels.push(days[i % 7]);
                }
                this.setState({dayLabels: labels});
        }

        let weeklyOrders = [0, 0, 0, 0, 0, 0, 0];
        const day = 24 * 60 * 60 * 1000;
        const today = currentDate.setHours(0, 0, 0, 0);
        let orderDate;
        for (let i=0; i < orders.length; i++) {
            orderDate = new Date(orders[i].date).setHours(0, 0, 0, 0);
            for (let j=0; j < 7; j++) {
                if (orderDate === today - j*day) {
                    weeklyOrders[weeklyOrders.length -1 - j] += 1;
                    break;
                }
            }
        }

        let options = dailySalesChart.options;
        const minPrice = Math.min.apply(Math, weeklyOrders);
        const maxPrice = Math.max.apply(Math, weeklyOrders);
        options.low = minPrice - (0.1 * minPrice);
        options.high = maxPrice + (0.1 * maxPrice);

        return {
         'data': {
             labels: this.state.dayLabels,
             series: [weeklyOrders]
         },
         'options': options
        }
    }

    render() {
        return (
            <div>
                <Grid container>
                    <ItemGrid xs={12} sm={6} md={3}>
                        <a href="/orders">
                        <ChartCard
                            chart={
                                <ChartistGraph
                                    className="ct-chart"
                                    data={this.getWeeklyOrderChartData(this.props.ordersList)['data']}
                                    type="Line"
                                    options={this.getWeeklyOrderChartData(this.props.ordersList)['options']}
                                    listener={dailySalesChart.animation}
                                />
                            }
                            chartColor="blue"
                            title="Weekly Orders"
                            text=""
                            statIcon={AccessTime}
                            statText=""
                        />
                        </a>
                    </ItemGrid>
                    <ItemGrid xs={12} sm={6} md={3}>
                        <a href="/wallet">
                        <StatsCard
                            icon={LocalAtm}
                            iconColor="green"
                            title="My tokens"
                            description={this.state.balance + ' LKD'}
                            statIcon={LocalAtm}
                            statText="Wallet"
                        />
                        </a>
                    </ItemGrid>
                    <ItemGrid xs={12} sm={6} md={3}>
                        <a href="/bids">
                        <StatsCard
                            icon={TrendingUp}
                            iconColor="green"
                            title="My open proposals"
                            description={this.props.myBidsNumber}
                            statIcon={TrendingUp}
                            statText="Bids"
                        />
                        </a>
                    </ItemGrid>
                </Grid>
                <Grid container>
                    <ItemGrid xs={12} sm={12} md={12}>
                        <OrdersTable
                            cardTitle="Latest Orders"
                            data={this.props.ordersList}
                            limitRows={5}
                        />
                    </ItemGrid>
                </Grid>
            </div>
        );
    }
}

Dashboard.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => {
    return {
        ordersList: state.ordersList,
        myBidsNumber: state.bidsList.filter(item => item.user_id === state.user.id).length
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchOrdersList: ({active}) => {
            dispatch(fetchOrdersList({active}));
        },
        fetchBidsList: ({active, aggregate}) => {
            dispatch(fetchBidsList({active, aggregate}));
        },
        checkBalance: () => {
            return dispatch(checkBalance());
        },
        showAlert: message => {
            dispatch(showAlert(message));
        },
        submitBid: (orderId, bidAmount) => {
            return dispatch(submitBid(orderId, bidAmount));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(dashboardStyle)(Dashboard));
