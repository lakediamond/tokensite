// ##############################
// // // Dashboard styles
// #############################

import { successColor } from "variables/styles";

const dashboardStyle = theme => ({
    successText: {
        color: successColor
    },
    upArrowCardCategory: {
        width: 14,
        height: 14
    },
    close: {
        width: theme.spacing.unit * 4,
        height: theme.spacing.unit * 4,
    },
});

export default dashboardStyle;
