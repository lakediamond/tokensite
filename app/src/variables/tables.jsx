const tableColumns = {
    'Orders': [
        { id: 'blockchain_id', numeric: true, disablePadding: true, label: 'ID' },
        { id: 'type', numeric: false, disablePadding: false, label: 'Type' },
        { id: 'filledPercentage', numeric: true, disablePadding: false, label: 'Filled %', suffix: '%' },
        { id: 'tmt_needed', numeric: true, disablePadding: false, label: 'Growth time', suffix: 'LKD' },
        { id: 'conversion', numeric: true, disablePadding: false, label: 'Price per token', suffix: 'CHF' },
        { id: 'order_price', numeric: true, disablePadding: false, label: 'Order Price', suffix: 'ETH' },
        { id: 'expiration', date: true, numeric: false, disablePadding: false, label: 'Expires on' },
    ],
    'Bids': [
        { id: 'order_blockchain_id', numeric: true, disablePadding: false, label: 'Order ID' },
        { id: 'amount', numeric: false, disablePadding: false, label: 'Amount (LKD)' },
        { id: '_actionElement', numeric: false, disablePadding: true, label: '', action: true },
    ],
    'History Bids': [
        { id: 'order_blockchain_id', numeric: true, disablePadding: false, label: 'Order ID' },
        { id: 'amount', numeric: false, disablePadding: false, label: 'Amount (LKD)' },
        { id: 'createdAt', date: true, numeric: false, disablePadding: false, label: 'Date' },
    ],
    'BidQueue': [
        { id: 'id', numeric: true, disablePadding: true, label: 'Queue Position' },
        { id: 'amount', numeric: true, disablePadding: false, label: 'Amount' },
        { id: 'createdAt', numeric: false, disablePadding: false, label: 'Date Opened' },
        { id: 'trust', numeric: false, disablePadding: false, label: 'Trustworthiness' },
        { id: 'status', numeric: false, disablePadding: false, label: 'Status' },
    ]
};

module.exports = {
    tableColumns
};
