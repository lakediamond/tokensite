import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableFooter, TablePagination, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import moment from 'moment';

import SortableTableHead from './SortableTableHead.js';

const styles = theme => ({
    root: {
        width: '100%',
    },
    table: {
        minWidth: 800,
    },
    tableNoMinWidth: {
        minWidth: 0
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    highlight: {
        backgroundColor: '#ddd !important',
        opacity: '0.6'
    }
});

class SortableTable extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'asc',
            orderBy: 'id',
            data: this.props.data,
            page: 0,
            rowsPerPage: 5,
        };
    }

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.props.handleSortChange(order, orderBy);

        this.setState({ order, orderBy });
    };

    handleRowClick = data => {
        if (this.props.onRowClick) {
            this.props.onRowClick(data);
        }
    }

    handleChangePage = (event, page) => {
        this.setState({ page });
    };

    handleChangeRowsPerPage = event => {
        this.setState({ rowsPerPage: event.target.value });
    };

    render() {
        const { classes } = this.props;
        const { order, orderBy, rowsPerPage, page } = this.state;
        const emptyRows = rowsPerPage - Math.min(rowsPerPage, this.props.data.length - page * rowsPerPage);

        return (
            <Paper className={classes.root}>
                <div className={classes.tableWrapper}>
                    <Table className={classes.table + (this.props.minWidth === false ? ' ' + classes.tableNoMinWidth : '')}>
                        <SortableTableHead
                            order={order}
                            orderBy={orderBy}
                            onRequestSort={this.handleRequestSort}
                            rowCount={this.props.data.length}
                            columns={this.props.columns}
                        />
                        <TableBody>
                            {this.props.data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n, index) => {
                                let highlighted = false;
                                if (this.props.highlight) {
                                    highlighted = this.props.highlight(n);
                                }

                                return (
                                    <TableRow
                                        hover
                                        onClick={this.handleRowClick.bind(this, n)}
                                        tabIndex={-1}
                                        className={highlighted ? classes.highlight : ''}
                                        key={page + ''+ index /* unique across all table pages */}
                                    >
                                        {this.props.columns.map((column, index) => {
                                            let content;
                                            if (column.boolean) {
                                                content = n[column.id] ? 'Yes' : 'No';
                                            } else if (column.date) {
                                                content = moment(n[column.id]).format('DD-MM-YYYY, HH:mm');
                                            } else {
                                                content = n[column.id];
                                            }

                                            return (
                                                <TableCell
                                                    key={index}
                                                    numeric={column.numeric}
                                                    padding={column.disablePadding ? 'none' : 'default'}
                                                >
                                                {content}
                                                {(column.suffix ? ' ' + column.suffix : '')}</TableCell>);
                                        })}
                                    </TableRow>
                                );
                            })}
                            {emptyRows > 0 && (
                                <TableRow style={{ height: 49 * emptyRows }}>
                                    <TableCell colSpan={6} />
                                </TableRow>
                            )}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    colSpan={6}
                                    count={this.props.data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </div>
            </Paper>
        );
    }
}

SortableTable.propTypes = {
    classes: PropTypes.object.isRequired,
    data: PropTypes.array.isRequired,
    columns: PropTypes.array.isRequired,
    onRowClick: PropTypes.func,
    highlight: PropTypes.func,
};

export default withStyles(styles)(SortableTable);
