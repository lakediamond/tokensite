import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import Menu, { MenuItem } from 'material-ui/Menu';
import { FormLabel } from 'material-ui/Form';

const styles = theme => ({
    root: {
        margin: "20px 0"
    },
    optionsRoot: {
        display: 'inline-block',
        backgroundColor: theme.palette.background.paper,
    },
});

class SimpleMenu extends React.Component {
    state = {
        anchorEl: null,
        selectedIndex: this.props.selectedIndex || 0,
    };

    button = undefined;

    handleClick = event => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleMenuItemClick = (event, index, value) => {
        this.setState({ selectedIndex: index, anchorEl: null });
        this.props.handleOptionChange(value);
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const { classes } = this.props;
        const { anchorEl } = this.state;

        return (
            <div className={classes.root}>
                <FormLabel component="legend">{this.props.label}</FormLabel>
                <div className={classes.optionsRoot}>
                    <Button
                        aria-owns={anchorEl ? 'simple-menu' : null}
                        aria-haspopup="true"
                        onClick={this.handleClick}
                    >
                        {this.props.options[this.state.selectedIndex].label}
                    </Button>
                    <Menu
                        id="lock-menu"
                        anchorEl={anchorEl}
                        open={Boolean(anchorEl)}
                        onClose={this.handleClose}
                    >
                        {this.props.options.map((option, index) => (
                            <MenuItem
                                key={index}
                                selected={index === this.state.selectedIndex}
                                onClick={event => this.handleMenuItemClick(event, index, option.name)}
                            >
                                {option.label}
                            </MenuItem>
                        ))}
                    </Menu>
                </div>
            </div>
        );
    }
}

SimpleMenu.propTypes = {
    classes: PropTypes.object.isRequired,
    label: PropTypes.string.isRequired,
    options: PropTypes.array.isRequired,
    selectedIndex: PropTypes.number
};

export default withStyles(styles)(SimpleMenu);
