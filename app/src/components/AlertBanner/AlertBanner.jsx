import React from "react";
import { withStyles } from 'material-ui';
import { connect } from 'react-redux';

import Contract from '../../lib/Contract';

const styles = {
    globalAlert: {
        textAlign: 'center',
        backgroundColor: '#f00',
        color: '#fff',
        fontWeight: 400,
    }
};

class AlertBanner extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            alertMessage: '',
        };
    }

    checkAddress () {
        try {
            const contractInstance = new Contract();
            const addressList =
                this.props.user.addressList.map(hash => hash.toLowerCase());

            if (!addressList.includes(contractInstance.myAddress.toLowerCase())) {
                this.setState({alertMessage: 'Your Metamask address is not registered, please fix this before bidding!'});
            }
            else {
                this.setState({alertMessage: ''});
            }
        } catch (e) {
            this.setState({alertMessage: e.message});
        }
        setTimeout(() => {this.checkAddress()}, 1000);
    }

    componentDidMount () {
        this.checkAddress();
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                {(this.state.alertMessage === '') ? null :
                    <div className={classes.globalAlert}>
                        {this.state.alertMessage}
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        user: state.user,
    };
};

export default connect(mapStateToProps, null)(withStyles(styles)(AlertBanner));
