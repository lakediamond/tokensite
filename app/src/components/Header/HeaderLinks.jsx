import React from "react";
import classNames from "classnames";
import { Manager, Target, Popper } from "react-popper";
import {
    withStyles,
    IconButton,
    MenuItem,
    MenuList,
    Grow,
    Paper,
    ClickAwayListener,
    Hidden
} from "material-ui";
import { Person } from "material-ui-icons";

import headerLinksStyle from "variables/styles/headerLinksStyle";

class HeaderLinks extends React.Component {
    state = {
        openMenu: false,
    };

    handleClick = (id) => {
        this.setState({ openMenu: !this.state.openMenu });
    };

    handleClose = () => {
        this.setState({ openMenu: false });
    };

    render() {
        const { classes } = this.props;
        const { openMenu } = this.state;
        return (
            <div>
                <Manager style={{ display: "inline-block", marginTop: "26px" }}>
                    <Target>
                        <IconButton
                            color="inherit"
                            aria-label="Person"
                            aria-owns={openMenu ? "menu-list" : null}
                            aria-haspopup="true"
                            onClick={() => {this.handleClick('menu')}}
                            className={classes.buttonLink}
                        >
                            <Person className={classes.links} />
                            <Hidden mdUp>
                                <p className={classes.linkText}>Menu</p>
                            </Hidden>
                        </IconButton>
                    </Target>
                    <Popper
                        placement="bottom-start"
                        eventsEnabled={openMenu}
                        className={
                            classNames({ [classes.popperClose]: !openMenu }) +
                            " " +
                            classes.pooperResponsive
                        }
                    >
                        <ClickAwayListener onClickAway={this.handleClose}>
                            <Grow
                                in={openMenu}
                                id="menu-list"
                                style={{ transformOrigin: "0 0 0" }}
                            >
                                <Paper className={classes.dropdown}>
                                    <MenuList role="menu">
                                        <MenuItem
                                            onClick={() => {
                                                this.handleClick('menu');
                                                localStorage.removeItem('user');
                                                this.props.history.push('/login');
                                            }}
                                            className={classes.dropdownItem}
                                        >
                                            Logout
                                        </MenuItem>
                                    </MenuList>
                                </Paper>
                            </Grow>
                        </ClickAwayListener>
                    </Popper>
                </Manager>
            </div>
        );
    }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
