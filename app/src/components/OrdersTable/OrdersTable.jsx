import React from "react";
import { connect } from 'react-redux'

import {
    RegularCard,
    SortableTable,
} from "components";
import {
    submitBid,
    showAlert
} from '../../actions';
import axios from '../../lib/axiosWrapper';
import OrderModal from './OrderModal';
import { tableColumns } from "variables/tables";
import { metaMaskLoadingMessage } from 'variables/general';
import config from '../../config.json';

class OrdersTable extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            order: 'desc',
            orderBy: 'blockchain_id',
            modal: {
                open: false,
                data: {},
            },
            orderTypes: [],
        };
    }

    handleSubmitBid = async ({bidAmount, blockchainOrderId}) => {
        bidAmount = parseInt(bidAmount, 10);
        if (isNaN(bidAmount) || bidAmount <= 0) {
            this.props.showAlert('Invalid bid amount.');
            return;
        }

        try {
            const txid = await this.props.submitBid({bidAmount, blockchainOrderId});

            this.props.showAlert(<div>Transaction created with hash: <a href={`${config.etherscan_path}/tx/${txid}`} target="_blank">{txid}</a></div>);
        } catch (e) {
            // modal will catch it
            throw e;
        }
    }

    fetchTypes = async () => {
        const { data } = await axios.get('/types');

        let types = {};
        data.forEach(item => (types[item.name] = item.label));

        this.setState({orderTypes: types});
    }

    componentWillMount() {
        this.fetchTypes();
    }

    handleSortChange = (order, orderBy) => {
        this.setState({orderBy, order});
    }

    toggleModal = (data) => {
        const modal = {
            open: !this.state.modal.open,
            data
        };

        this.setState({modal});
    }

    handleRowClick = data => {
        this.toggleModal(data);
    }

    render() {
        let data = JSON.parse(JSON.stringify(this.props.data));

        if (this.state.order === 'asc') {
            data.sort((a, b) => (a[this.state.orderBy] < b[this.state.orderBy] ?  -1 : 1));
        } else {
            data.sort((a, b) => (a[this.state.orderBy] < b[this.state.orderBy] ?  1 : -1));
        }

        if (this.props.limitRows) {
            data = data.slice(0, this.props.limitRows);
        }

        data.forEach(item => item.type = this.state.orderTypes[item.type] || item.type);

        return (
            <div>
                <RegularCard
                    headerColor="orange"
                    cardTitle={this.props.cardTitle}
                    content={
                        <SortableTable
                            data={data}
                            columns={tableColumns['Orders']}
                            onRowClick={this.handleRowClick}
                            handleSortChange={this.handleSortChange} />
                    }
                />

                <OrderModal
                    loadingMessage={metaMaskLoadingMessage}
                    onSubmit={this.handleSubmitBid}
                    open={!!this.state.modal.open}
                    onClose={this.toggleModal.bind(this, {})}
                    data={this.state.modal.data}
                    actions={this.props.actions}
                />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        showAlert: message => {
            dispatch(showAlert(message));
        },
        submitBid: (orderId, bidAmount) => {
            return dispatch(submitBid(orderId, bidAmount));
        }
    };
};

export default connect(null, mapDispatchToProps)(OrdersTable);
