import React from 'react';
import Input, { InputAdornment } from 'material-ui/Input';
import moment from 'moment';

import Modal from '../Modal/Modal';
import platelet from '../../assets/img/platelets.png';
import jewellery from '../../assets/img/jewellery.png';
import transistors from '../../assets/img/transistors.png';
import photonics from '../../assets/img/photonics.png';
import micromechanical from '../../assets/img/micromechanical.png';

class OrderModal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            bidAmount: 0,
            blockchainOrderId: props.data.blockchain_id
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            bidAmount: 0,
            blockchainOrderId: nextProps.data.blockchain_id
        });
    }

    shouldComponentUpdate(nextProps) {
        return JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data);
    }

    handleInputChange = event => {
        this.setState({bidAmount: event.target.value});
        this.forceUpdate();
    }

    render() {
        const data = [
            {
                title: 'Blockchain Order ID',
                value: this.props.data.blockchain_id,
            },
            {
                title: 'Order Type',
                value: this.props.data.type,
            },
            {
                title: 'Filled %',
                value: this.props.data.filled + '%',
            },
            {
                title: 'Growth time',
                value: this.props.data.tmt_needed,
            },
            {
                title: 'Order Price',
                value: (this.props.data.order_price ? this.props.data.order_price : 0) + ' ETH',
            },
            {
                title: 'Price per token',
                value: (this.props.data.conversion ? this.props.data.conversion : 0) + ' CHF',
            },
            {
                title: 'Created',
                value: moment(this.props.data.date).format('DD-MM-YYYY, HH:mm'),
            },
            {
                title: 'Open Until',
                value: moment(this.props.data.expiration).format('DD-MM-YYYY, HH:mm'),
            },
            {
                title: 'Bid Amount',
                value: '',
                action: (
                    <Input
                        type='number'
                        onChange={this.handleInputChange}
                        endAdornment={<InputAdornment position="end">LKD</InputAdornment>}
                        inputProps={{
                            min: 0,
                            max: this.props.data.tmt_needed,
                            value: this.state.bidAmount
                        }} />
                )
            },
        ];

        let image;
        switch (this.props.data.type) {
            case 'Platelet':
                image = platelet;
                break;
            case 'Round Brilliant':
                image = jewellery;
                break;
            case 'Platelet for transistors':
                image = transistors;
                break;
            case 'Platelet for photonics':
                image = photonics;
                break;
            case 'Platelet for lasers':
                image = photonics;
                break;
            case 'Platelet for micromechanical parts':
                image = micromechanical;
                break;
            default:
                break;
        }

        const props = {
            ...this.props,
            data,
            image,
            action: 'Bid Now',
            title: 'Order Details',
            actionData: {
                bidAmount: this.state.bidAmount,
                blockchainOrderId: this.state.blockchainOrderId
            }
        };

        return (
            <Modal {...props} />
        );
    }
}

export default OrderModal;
