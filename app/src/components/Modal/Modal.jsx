import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux'
import { withStyles } from 'material-ui/styles';
import List, { ListItem, ListItemText, ListItemSecondaryAction } from 'material-ui/List';
import Dialog, { DialogTitle, DialogContent } from 'material-ui/Dialog';
import { Button, ItemGrid } from 'components';
import { CircularProgress } from 'material-ui/Progress';
import Grid from 'material-ui/Grid';
import { primaryColor } from '../../variables/styles';
import { showAlert } from '../../actions';

const styles = theme => ({
    close: {
        width: theme.spacing.unit * 4,
        height: theme.spacing.unit * 4,
    },
    action: {
        marginTop: '-19px'
    },
    wrapper: {
        position: 'relative',
    },
    buttonProgress: {
        color: primaryColor,
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    imagePreview: {
        display: 'block',
        margin: 'auto',
        height: '120px',
        width: 'auto',
    },
    loadingMessage: {
        textAlign: 'center',
        fontWeight: 'bold'
    }
});

class Modal extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            actionPending: false,
        }
    }

    submitAction = async () => {
        const state = this.state;

        if (state.actionPending) {
            return false;
        }

        try {
            this.setState({actionPending: true});

            await this.props.onSubmit(this.props.actionData);

            state.actionPending = false;
            this.props.onClose();
        } catch(e) {
            this.props.showAlert(e.message);
            state.actionPending = false;
        }

        this.setState(state);
    }

    handleKeyDown = e => {
        if (e.keyCode === 13) {
            this.submitAction();
        }
    }

    render() {
        const {
            data,
            image,
            open,
            onClose,
            classes,
            title,
            action,
            actions
        } = this.props;

        return (
            <Dialog onKeyDown={this.handleKeyDown} onClose={onClose} open={open} id={"dialog-" + data.id}>
                <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    {(image !== null) ? (
                        <img src={image} alt={data.type} className={classes.imagePreview}/>
                    ) : null}
                    <List>
                        <Grid container>
                        {data.map((item, index) => (
                            <ItemGrid key={index} xs={12} sm={12} md={6}>
                                <ListItem>
                                    <ListItemText primary={item.title} secondary={item.value} />
                                    {item.action ? (
                                    <ListItemSecondaryAction className={classes.action}>
                                        {item.action}
                                    </ListItemSecondaryAction>
                                    ) : null}
                                </ListItem>
                            </ItemGrid>
                        ))}
                        </Grid>
                    </List>
                    {(typeof actions === 'undefined'
                        || actions === true) ? (
                        this.props.loadingMessage && this.state.actionPending ? (
                            <div className={classes.wrapper}>
                                <p className={classes.loadingMessage}>
                                    {this.props.loadingMessage}
                                </p>
                            </div>
                        ) : (
                            <div className={classes.wrapper}>
                                <Button
                                    disabled={this.state.actionPending}
                                    onClick={this.submitAction}
                                    fullWidth
                                    color="primary"
                                >
                                    {action}
                                </Button>
                                {this.state.actionPending && <CircularProgress size={24} className={classes.buttonProgress} />}
                            </div>
                        )
                    ) : null}
                </DialogContent>
            </Dialog>
        );
    }
}

Modal.propTypes = {
    open: PropTypes.bool.isRequired,
    data: PropTypes.array.isRequired,
    onClose: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    actions: PropTypes.bool,
    action: PropTypes.string,
    actionData: PropTypes.any,
    loadingMessage: PropTypes.string,
};
const mapDispatchToProps = dispatch => {
    return {
        showAlert: message => {
            dispatch(showAlert(message));
        },
    };
};

export default connect(null, mapDispatchToProps)(withStyles(styles)(Modal));
