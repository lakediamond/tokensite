import contractAbi from './abi';
import config from '../config.json';

const CONTRACT_ADDRESS = config.contract.address;

class NoMetamaskError extends Error {
    constructor(...params) {
        super(...params);
        this.message = 'MetaMask must be installed and enabled.';
    }
}

class GenericTransactionError extends Error {
    constructor(...params) {
        super(...params);
        this.message = 'There was an error processing the transaction. Try again.';
    }
}

class TransactionDeniedError extends Error {
    constructor(...params) {
        super(...params);
        this.message = 'The transaction was denied.';
    }
}

class NoETHWalletsError extends Error {
    constructor(...params) {
        super(...params);
        this.message = 'No available ETH wallets were found on MetaMask.';
    }
}

class Contract {
    constructor() {
        /*global web3 Web3:true*/

        if (!window['web3'] || !web3.currentProvider.isMetaMask) {
            throw new NoMetamaskError();
        }
        const web3Instance = new Web3(web3.currentProvider);

        web3Instance.eth.defaultAccount = web3Instance.eth.accounts[0];
        if (!web3Instance.eth.accounts.length) {
            throw new NoETHWalletsError();
        }

        const contractApi = web3Instance.eth.contract(contractAbi);
        this.contractInstance = contractApi.at(CONTRACT_ADDRESS);

        this.myAddress = web3Instance.eth.accounts[0];
        this.web3Instance = web3Instance;
    }

    callMethod(method, ...args) {
        return new Promise((resolve, reject) => {
            this.web3Instance.eth.getGasPrice((err, res) => {
                if (err) {
                    return reject(new GenericTransactionError());
                }

                let gasPrice = res + 1;
                this.contractInstance[method](...args, {gasPrice: gasPrice}, (err, result) => {
                    if (err) {
                        let error;
                        if (err.message.includes('User denied')) {
                            error = new TransactionDeniedError();
                        } else {
                            error = new GenericTransactionError();
                        }

                        return reject(error);
                    }

                    return resolve(result);
                });
            });
        });
    }

    bid(orderId, bidAmount) {
        return this.callMethod('bid', orderId, bidAmount);
    }

    withdraw(orderId, withdrawAmount) {
        return this.callMethod('withdraw', orderId, withdrawAmount);
    }

    async checkBalance() {
        // typecast as number
        return +(await this.callMethod('balanceOf', this.myAddress));
    }
}

export default Contract;
