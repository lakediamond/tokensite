const axios = require('axios');

const BASE_URL = process.env.REACT_APP_API_URL;

const wrapper = async (method, url, args) => {
    url = BASE_URL + url;
    return await axios[method].apply(this, [url, ...args]);
};

const axiosWrapper = {
    get: (url, ...args) => wrapper('get', url, args),
    delete: (url, ...args) => wrapper('delete', url, args),
    head: (url, ...args) => wrapper('head', url, args),
    options: (url, ...args) => wrapper('options', url, args),
    post: (url, ...args) => wrapper('post', url, args),
    put: (url, ...args) => wrapper('put', url, args),
    patch: (url, ...args) => wrapper('patch', url, args),
}

export default axiosWrapper;
