import axios from '../lib/axiosWrapper';

const requestOrdersList = () => {
    return { type: 'REQUEST_ORDERS_LIST' };
};

const fetchOrdersList = ({active}) => {
    return async dispatch => {
        dispatch(requestOrdersList());

        const res = await axios.get(`/orders?active=${+active}`);

        dispatch(receiveOrdersList(res.data));
    }
};

const receiveOrdersList = (data) => {
    return { type: 'RECEIVE_ORDERS_LIST', data };
};

export {
    fetchOrdersList,
};
