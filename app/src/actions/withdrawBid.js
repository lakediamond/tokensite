import Contract from '../lib/Contract';
import axios from '../lib/axiosWrapper';

export default ({blockchainOrderId, withdrawAmount}) => {
    return async dispatch => {
        const contractInstance = new Contract();

        const txId = await contractInstance.withdraw(blockchainOrderId, withdrawAmount);

        const bid = {
            blockchainOrderId,
            amount: -withdrawAmount,
            address: contractInstance.myAddress,
            txId,
        };

        try {
            await axios.post('/bids', bid);
        } catch(e) {
            // It is ok if the pending bid fails to be saved.
            // We will catch the event from the blockchain in any case.
            console.log(e);
        }

        return txId;
    }
};
