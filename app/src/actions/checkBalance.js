import Contract from '../lib/Contract';

export default () => {
    return dispatch => {
        const contractInstance = new Contract();
        return contractInstance.checkBalance();
    }
};
