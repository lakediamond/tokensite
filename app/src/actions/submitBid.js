import Contract from '../lib/Contract';
import axios from '../lib/axiosWrapper';

export default ({blockchainOrderId, bidAmount}) => {
    return async dispatch => {
        const contractInstance = new Contract();

        const txId = await contractInstance.bid(blockchainOrderId, bidAmount);

        const bid = {
            blockchainOrderId,
            amount: +bidAmount,
            address: contractInstance.myAddress,
            txId,
        };

        try {
            await axios.post('/bids', bid);
        } catch(e) {
            // It is ok if the pending bid fails to be saved.
            // We will catch the event from the blockchain in any case.
            console.log(e);
        }

        return txId;
    }
};
