let hideTimeout;

const closeAlert = () => {
    clearTimeout(hideTimeout);
    return { type: 'CLOSE_ALERT' };
};

const showAlert = message => {
    return dispatch => {
        clearTimeout(hideTimeout);
        hideTimeout = setTimeout(dispatch.bind({}, closeAlert()), 7000);

        dispatch({ type: 'SHOW_ALERT', message });
    }
};

export {
    closeAlert,
    showAlert,
};
