const loginUser = ({id, email, firstName, lastName, addressList}) => {
    localStorage.setItem('user', JSON.stringify({id, email, firstName, lastName, addressList}));

    return { type: 'LOGIN_USER', data: {id, email, firstName, lastName, addressList} };
};

export {
    loginUser
};
