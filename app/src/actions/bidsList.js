import axios from '../lib/axiosWrapper';

const requestQueuedBidsList = () => {
    return { type: 'REQUEST_QUEUED_BIDS_LIST' };
};

const receiveQueuedBidsList = (data) => {
    return { type: 'RECEIVE_QUEUED_BIDS_LIST', data };
};

const requestBidsList = () => {
    return { type: 'REQUEST_BIDS_LIST' };
};

const receiveBidsList = (data) => {
    return { type: 'RECEIVE_BIDS_LIST', data };
};

const fetchQueuedBidsList = () => {
    return async dispatch => {
        dispatch(requestQueuedBidsList());

        const res = await axios.get('/queuedbids');

        dispatch(receiveQueuedBidsList(res.data));
    }
};

const fetchBidsList = ({active, aggregate}) => {
    return async dispatch => {
        dispatch(requestBidsList());

        const [bidsResponse, pendingBidsResponse] = await Promise.all([
            axios.get(`/bids?active=${+active}&aggregate=${+aggregate}`),
            axios.get(`/bids?active=${+active}&status=pending`)
        ]);

        dispatch(receiveBidsList(
            pendingBidsResponse.data.concat(
                bidsResponse.data)
        ));
    }
};

export {
    fetchBidsList,
    fetchQueuedBidsList
};
