import DashboardPage from "views/Dashboard/Dashboard.jsx";
import OrdersList from "views/OrdersList/OrdersList.jsx";
import Bids from "views/Bids/Bids.jsx";
import History from "views/History/History.jsx";
import Wallet from "views/Wallet/Wallet.jsx";
import Settings from "views/Settings/Settings.jsx";

import {
    Dashboard,
    TrendingUp,
    Queue,
    LocalAtm,
    EventNote,
    Settings as SettingsIcon,
} from "material-ui-icons";

const appRoutes = [
    {
        path: "/dashboard",
        sidebarName: "Dashboard",
        navbarName: "Dashboard",
        icon: Dashboard,
        component: DashboardPage
    },
    {
        path: "/orders",
        sidebarName: "Orders",
        navbarName: "Orders",
        icon: TrendingUp,
        component: OrdersList
    },
    {
        path: "/bids",
        sidebarName: "Proposals",
        navbarName: "Active Proposals",
        icon: Queue,
        component: Bids
    },
    {
        path: "/wallet",
        sidebarName: "Wallet",
        navbarName: "Wallet",
        icon: LocalAtm,
        component: Wallet
    },
    {
        path: "/history",
        sidebarName: "History",
        navbarName: "History",
        icon: EventNote,
        component: History
    },
    {
        path: "/settings",
        sidebarName: "Settings",
        navbarName: "Settings",
        icon: SettingsIcon,
        component: Settings
    },
    { redirect: true, path: "/", to: "/dashboard", navbarName: "Redirect" }
];

export default appRoutes;
