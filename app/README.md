The front-end for the Lake Diamond token site, written in react.

# Running
* `npm install`
* Modify src/actions/index.js to include the correct `BASE_URL` for the URL of
  the API, e.g. 'http://lakediamond-site.com/api'.
* `npm start`
